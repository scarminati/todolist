﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ToDoList
{
    public partial class Form_Record : Form
    {
        private int indexRecordToEdit = -1;
        private Utente user;
        private String reparto;
        private bool justUpdate = false;
        private string statoLavoroPrimaDellaModifica = "";
        private string oreLavoratePrimaDellaModifica = "";
        private DateTime tempoInizioPrimaDellaModifica;

        public Form_Record()
        {
            try
            {
                InitializeComponent();
                DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [AutoTest].[dbo].[Utente2] WHERE [idUtente] IN (Select [idUtente] FROM [Autotest].[dbo].[Applicativo] WHERE [Autotest].[dbo].[Applicativo].[nomeApplicativo] = 'ToDoList')");
                cmb_assegnatoA.Items.Clear();
                foreach (DataRow row in dt.Rows)
                    cmb_assegnatoA.Items.Add(row["nome"].ToString() + " " + row["cognome"].ToString());
                this.DialogResult = DialogResult.Cancel;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public Form_Record(Utente user, String reparto) : this()
        {
            try
            {
                this.user = user;
                this.reparto = reparto;
                cmb_stato.SelectedIndex = 1;//di default, metto in pausa
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Overload del costruttore con 0 parametri, richiamato tramite :this()
        /// </summary>
        /// <param name="indexRecordToEdit"></param>
        public Form_Record(Utente user, int indexRecordToEdit, bool isCloned, bool justUpdate) : this()
        {
            try
            {
                this.user = user;
                this.indexRecordToEdit = indexRecordToEdit;
                this.justUpdate = justUpdate;
                if (isCloned)
                    this.indexRecordToEdit = -1;
                fillForm();
                if (justUpdate)
                    setRecordInPausa();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void fillForm()
        {
            try
            {
                DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [ToDoList] WHERE [ID] = '" + indexRecordToEdit.ToString() + "'");
                if (!dt.Rows.Count.Equals(0))
                {
                    if (!justUpdate)
                    {
                        tb_descrizioneTask.Text = dt.Rows[0].Field<string>("Task").ToString();
                        dtp_dataAssegnazione.Text = dt.Rows[0].Field<DateTime>("Data Assegnazione").ToShortDateString();
                        dtp_dataScadenza.Text = dt.Rows[0].Field<DateTime>("Data Scadenza").ToShortDateString();
                        tb_orePreventivate.Text = dt.Rows[0].Field<string>("Ore Preventivate hhh:mm").ToString();
                        cmb_assegnatoA.SelectedItem = dt.Rows[0].Field<string>("Assegnato A");
                        cmb_priorita.SelectedItem = dt.Rows[0].Field<string>("Priorità");
                        cmb_stato.SelectedItem = dt.Rows[0].Field<string>("Stato");
                        tb_note.Text = dt.Rows[0].Field<string>("Note");
                    }
                    statoLavoroPrimaDellaModifica = dt.Rows[0].Field<string>("Stato");
                    oreLavoratePrimaDellaModifica = dt.Rows[0].Field<string>("Ore Lavorate hhh:mm").ToString();
                    tempoInizioPrimaDellaModifica = dt.Rows[0].Field<DateTime>("Inizio Lavoro");
                }
                else
                {
                    dtp_dataAssegnazione.Text = DateTime.Now.ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void brn_OK_Click(object sender, EventArgs e)
        {
            try
            {
                if (indexRecordToEdit.Equals(-1))
                    searchRecordInDB();
                else
                    updateRecordInDB();
                if (this.DialogResult.Equals(DialogResult.OK))
                    this.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void searchRecordInDB()
        {
            try
            {
                DataTable dt = Utility.getDataTableFromDB("SELECT * FROM [ToDoList]  WHERE " +
                    "[Task] = '" + tb_descrizioneTask.Text + "' AND " +
                    "[Assegnato A] = '" + (cmb_assegnatoA.SelectedItem == null ? "" : cmb_assegnatoA.SelectedItem.ToString())  + "'");
                if (!dt.Rows.Count.Equals(0))
                {
                    DialogResult result = MessageBox.Show("Il record già presente nel Database verrà aggiornato con i dati immessi nel form.\nSei sicuro di voler procedere?", "Aggiornamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result.Equals(DialogResult.Yes))
                    {
                        indexRecordToEdit = dt.Rows[0].Field<int>(0);
                        updateRecordInDB();
                        this.DialogResult = DialogResult.OK;
                    }
                    else
                        this.DialogResult = DialogResult.Abort;
                }
                else
                    insertRecordInDB();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void insertRecordInDB()
        {
            try
            {
                string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff");
                Utility.executeQuery("INSERT INTO [ToDoList] " +
                "([Reparto],[Task],[Data Assegnazione],[Data Scadenza],[Ore Preventivate hhh:mm],[Ore Lavorate hhh:mm],[Inizio Lavoro],[Fine Lavoro],Priorità,Stato,[Assegnato A],Note,[Data Aggiornamento],[Aggiornato Da]) " +
                "VALUES ('" + reparto + "'," +
                    "'" + tb_descrizioneTask.Text.ToString().Replace("'","''") + "'," +
                    "'" + dtp_dataAssegnazione.Value.ToShortDateString() + "'," +
                    "'" + dtp_dataScadenza.Value.ToShortDateString() + "'," +
                    "'" + tb_orePreventivate.Text.ToString() + "'," +
                    "'000:00'," + //Ore Lavorate di default
                    "'" + date + "'," +//Data di inizio lavoro
                    "'" + date + "'," +//Data di fine lavoro
                    "'" + (cmb_priorita.SelectedItem == null ? "" : cmb_priorita.SelectedItem.ToString()) + "'," +
                    "'" + (cmb_stato.SelectedItem == null ? "" : cmb_stato.SelectedItem.ToString()) + "'," +
                    "'" + (cmb_assegnatoA.SelectedItem == null ? "" : cmb_assegnatoA.SelectedItem.ToString()) + "'," +
                    "'" + tb_note.Text + "'," +
                    "'" + date + "'," +
                    "'" + user.Nome + " " + user.Cognome + "')");
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setRecordInPausa()
        {
            List<string> tempoLavorato = calcolaOreLavorate();
            Utility.executeQuery("UPDATE [ToDoList] SET " +
                "[Stato] = 'In Pausa' ," +
                "[Ore Lavorate hhh:mm] = '" + tempoLavorato[0] + ":" + tempoLavorato[1] + "'," +
                "[Fine Lavoro] = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "' " +
                "WHERE [Id] = '" + indexRecordToEdit.ToString() + "' AND [Aggiornato Da] = '" + user.Nome + " " + user.Cognome + "'");
        }

        private List<string> calcolaOreLavorate()
        {
            List<String> retValue = new List<string>();
            TimeSpan tempoLavorato = DateTime.Now.Subtract(tempoInizioPrimaDellaModifica);
            char[] separator = { ':' };
            string[] arrayString = oreLavoratePrimaDellaModifica.Split(separator[0]);
            int oreLavorate = Convert.ToInt32(arrayString[0]);
            int minutiLavorati = Convert.ToInt32(arrayString[1]) + tempoLavorato.Minutes;
            if (minutiLavorati > 60)
            {
                minutiLavorati = minutiLavorati - 60;
                oreLavorate++;
            }
            oreLavorate = oreLavorate + tempoLavorato.Hours;
            retValue.Add(oreLavorate.ToString().PadLeft(3, '0'));
            retValue.Add(minutiLavorati.ToString().PadLeft(2, '0'));
            return (retValue);
        }

        private void updateRecordInDB()
        {
            try
            {
                Utility.executeQuery("UPDATE [ToDoList] SET " +
                "[Task] = '" + tb_descrizioneTask.Text.ToString().Replace("'", "''") + "'," +
                "[Data Assegnazione] = '" + dtp_dataAssegnazione.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                "[Data Scadenza] = '" + dtp_dataScadenza.Value.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                "[Ore Preventivate hhh:mm] = '" + tb_orePreventivate.Text.ToString() + "'," +
                "[Priorità] = '" + (cmb_priorita.SelectedItem == null ? "" : cmb_priorita.SelectedItem.ToString()) + "'," +
                "[Stato] = '" + (cmb_stato.SelectedItem == null ? "" : cmb_stato.SelectedItem.ToString()) + "'," +
                "[Assegnato A] = '" + (cmb_assegnatoA.SelectedItem == null ? "" : cmb_assegnatoA.SelectedItem.ToString()) + "'," +
                "[Note] = '" + tb_note.Text + "'," +
                "[Data Aggiornamento] = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "'," +
                "[Aggiornato Da] = '" + user.Nome + " " + user.Cognome + "' " +
                "WHERE [Id] = '" + indexRecordToEdit.ToString() + "'");
                if (!statoLavoroPrimaDellaModifica.Equals(cmb_stato.SelectedItem.ToString()))
                {
                    switch (statoLavoroPrimaDellaModifica)
                    {
                        case "In Lavorazione":
                            {
                                //Prima ero in stato In Lavorazione; Adesso sono in stato In Pausa o Completato
                                List<string> tempoLavorato = calcolaOreLavorate();
                                Utility.executeQuery("UPDATE [ToDoList] SET " +
                                "[Ore Lavorate hhh:mm] = '" + tempoLavorato[0] + ":" + tempoLavorato[1] + "'," +
                                "[Fine Lavoro] = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "' " +
                                "WHERE [Id] = '" + indexRecordToEdit.ToString() + "'");
                            }
                            break;
                        default:
                            {
                                //Prima ero in stato In Pausa o Completato; Adesso sono in stato In Lavorazione
                                string tempoInizioLavoro = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fffffff");
                                Utility.executeQuery("UPDATE [ToDoList] SET " +
                                "[Inizio Lavoro] = '" + tempoInizioLavoro + "'," +
                                "[Fine Lavoro] = '" + tempoInizioLavoro + "' " +
                                "WHERE [Id] = '" + indexRecordToEdit.ToString() + "'");
                            }
                            break;
                    }
                }
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
