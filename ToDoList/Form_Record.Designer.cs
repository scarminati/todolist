﻿namespace ToDoList
{
    partial class Form_Record
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Record));
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_orePreventivate = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmb_stato = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmb_priorita = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dtp_dataScadenza = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.cmb_assegnatoA = new System.Windows.Forms.ComboBox();
            this.dtp_dataAssegnazione = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tb_note = new System.Windows.Forms.TextBox();
            this.brn_OK = new System.Windows.Forms.Button();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tb_descrizioneTask = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data Assegnazione";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_orePreventivate);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmb_stato);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.cmb_priorita);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.dtp_dataScadenza);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cmb_assegnatoA);
            this.groupBox1.Controls.Add(this.dtp_dataAssegnazione);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(12, 208);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(483, 167);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dettagli Task";
            // 
            // tb_orePreventivate
            // 
            this.tb_orePreventivate.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.tb_orePreventivate.Location = new System.Drawing.Point(346, 55);
            this.tb_orePreventivate.Mask = "000:00";
            this.tb_orePreventivate.Name = "tb_orePreventivate";
            this.tb_orePreventivate.Size = new System.Drawing.Size(121, 20);
            this.tb_orePreventivate.TabIndex = 4;
            this.tb_orePreventivate.Text = "00000";
            this.tb_orePreventivate.ValidatingType = typeof(System.DateTime);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(354, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 26);
            this.label1.TabIndex = 13;
            this.label1.Text = "Tempo di Lavoro \r\nPreventivato (hhh:mm)";
            // 
            // cmb_stato
            // 
            this.cmb_stato.FormattingEnabled = true;
            this.cmb_stato.Items.AddRange(new object[] {
            "In Lavorazione",
            "In Pausa",
            "Completato"});
            this.cmb_stato.Location = new System.Drawing.Point(346, 128);
            this.cmb_stato.Name = "cmb_stato";
            this.cmb_stato.Size = new System.Drawing.Size(121, 21);
            this.cmb_stato.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(385, 104);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Stato";
            // 
            // cmb_priorita
            // 
            this.cmb_priorita.FormattingEnabled = true;
            this.cmb_priorita.Items.AddRange(new object[] {
            "0 - Bassa",
            "1 - Media",
            "2 - Alta",
            "3 - Top"});
            this.cmb_priorita.Location = new System.Drawing.Point(173, 128);
            this.cmb_priorita.Name = "cmb_priorita";
            this.cmb_priorita.Size = new System.Drawing.Size(121, 21);
            this.cmb_priorita.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(209, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Priorità";
            // 
            // dtp_dataScadenza
            // 
            this.dtp_dataScadenza.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_dataScadenza.Location = new System.Drawing.Point(173, 55);
            this.dtp_dataScadenza.Name = "dtp_dataScadenza";
            this.dtp_dataScadenza.Size = new System.Drawing.Size(121, 20);
            this.dtp_dataScadenza.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(187, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Data Scadenza";
            // 
            // cmb_assegnatoA
            // 
            this.cmb_assegnatoA.FormattingEnabled = true;
            this.cmb_assegnatoA.Items.AddRange(new object[] {
            "Accettazione",
            "Analisi",
            "In Attesa Di Preventivo",
            "In Riparazione",
            "Riparato",
            "Da Insacchettare",
            "Fine Lavorazione"});
            this.cmb_assegnatoA.Location = new System.Drawing.Point(15, 128);
            this.cmb_assegnatoA.Name = "cmb_assegnatoA";
            this.cmb_assegnatoA.Size = new System.Drawing.Size(121, 21);
            this.cmb_assegnatoA.TabIndex = 5;
            // 
            // dtp_dataAssegnazione
            // 
            this.dtp_dataAssegnazione.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtp_dataAssegnazione.Location = new System.Drawing.Point(15, 55);
            this.dtp_dataAssegnazione.Name = "dtp_dataAssegnazione";
            this.dtp_dataAssegnazione.Size = new System.Drawing.Size(121, 20);
            this.dtp_dataAssegnazione.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(29, 104);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Assegnato A";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tb_note);
            this.groupBox3.Location = new System.Drawing.Point(12, 381);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(483, 190);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Note Task";
            // 
            // tb_note
            // 
            this.tb_note.Location = new System.Drawing.Point(20, 26);
            this.tb_note.Multiline = true;
            this.tb_note.Name = "tb_note";
            this.tb_note.Size = new System.Drawing.Size(447, 150);
            this.tb_note.TabIndex = 8;
            // 
            // brn_OK
            // 
            this.brn_OK.ImageKey = "ok.png";
            this.brn_OK.ImageList = this.imageList;
            this.brn_OK.Location = new System.Drawing.Point(202, 592);
            this.brn_OK.Name = "brn_OK";
            this.brn_OK.Size = new System.Drawing.Size(50, 50);
            this.brn_OK.TabIndex = 9;
            this.brn_OK.UseVisualStyleBackColor = true;
            this.brn_OK.Click += new System.EventHandler(this.brn_OK_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "save.png");
            this.imageList.Images.SetKeyName(1, "export.png");
            this.imageList.Images.SetKeyName(2, "ok.png");
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tb_descrizioneTask);
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(483, 190);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Descrizione Task";
            // 
            // tb_descrizioneTask
            // 
            this.tb_descrizioneTask.Location = new System.Drawing.Point(20, 26);
            this.tb_descrizioneTask.Multiline = true;
            this.tb_descrizioneTask.Name = "tb_descrizioneTask";
            this.tb_descrizioneTask.Size = new System.Drawing.Size(447, 150);
            this.tb_descrizioneTask.TabIndex = 1;
            // 
            // Form_Record
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 654);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.brn_OK);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form_Record";
            this.Text = "Aggiungi Task";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtp_dataAssegnazione;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cmb_assegnatoA;
        private System.Windows.Forms.TextBox tb_note;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button brn_OK;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tb_descrizioneTask;
        private System.Windows.Forms.ComboBox cmb_stato;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmb_priorita;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtp_dataScadenza;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox tb_orePreventivate;
    }
}

