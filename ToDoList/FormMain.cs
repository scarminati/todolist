﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Threading;

namespace ToDoList
{
    public partial class Form_Main : Form
    {
        private string selectCondition = "";
        private Utente user;
        private bool changeColorLines = false;
        private Color colorLines = Color.White;
        public Form_Main(Utente user)
        {
            try
            {
                InitializeComponent();
                this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                setApplicationVersion();
                this.user = user;
                lbl_NomeUtente.Text = user.Nome + " " + user.Cognome;
                tabControlList_SelectedIndexChanged(null, null);
                if (ConfigurationManager.AppSettings["LOCAL_TEST"].Equals("false"))
                {
                    bool updatesAvailable = Utility.checkForUpdates();
                    if (updatesAvailable)
                    {
                        btn_Updates.Visible = true;
                    }
                }
                setDataGridViewLayout();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tabControlList_SelectedIndexChanged(object sender, EventArgs e)
        {
            setDataGridViewLayout();
        }

        private void Form_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            resetCountUp();
        }

        private void resetCountUp()
        {
            try
            {
                DataTable dt = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] WHERE Stato = 'In Lavorazione'");
                if (!dt.Rows.Equals(0))
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        Form_Record formRec = new Form_Record(user, Convert.ToInt32(row["ID"]), false, true);
                    }
                }
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region MetodiGenerali

        private void setApplicationVersion()
        {
            // considero solo i primi 2 valori per la versione da visualizzare
            String tmp = Application.ProductVersion;
            int pos = tmp.IndexOf(".");
            pos = tmp.IndexOf(".", pos + 1);

            this.Text += " - V. " + tmp.Substring(0, pos);
        }

        /// <summary>
        /// Ripristina il valore di default della textbox di ricerca e tutte le datagridview
        /// </summary>
        /// <param name="tb"></param>
        private void resetSearchTextboxAndDatagridviews(ToolStripTextBox tb)
        {
            if (tstb_SearchTaskNewProject.Text.Equals(""))
            {
                tstb_SearchTaskNewProject.Text = "Cerca...";
            }
            if (tstb_SearchTaskRepMeccanico.Text.Equals(""))
            {
                tstb_SearchTaskRepMeccanico.Text = "Cerca...";
            }
            if (tstb_SearchTaskRepElettrico.Text.Equals(""))
            {
                tstb_SearchTaskRepElettrico.Text = "Cerca...";
            }
            if (tstb_SearchTaskRepSoftware.Text.Equals(""))
            {
                tstb_SearchTaskRepSoftware.Text = "Cerca...";
            }
            if (tstb_SearchTaskRepQualita.Text.Equals(""))
            {
                tstb_SearchTaskRepQualita.Text = "Cerca...";
            }
            if (tstb_SearchTaskCompleted.Text.Equals(""))
            {
                tstb_SearchTaskCompleted.Text = "Cerca...";
            }
            if (tstb_SearchTaskAll.Text.Equals(""))
            {
                tstb_SearchTaskAll.Text = "Cerca...";
            }
            if (tstb_SearchNotePersonali.Text.Equals(""))
            {
                tstb_SearchNotePersonali.Text = "Cerca...";
            }
            //aggiungere le atre ricerche QUI...
            tabControlList_SelectedIndexChanged(null, null);
        }

        /// <summary>
        /// modifica la visualizzazione dei dati nella datagridview in accordo con la selezione scritta nella corrispondente textbox di ricerca
        /// </summary>
        /// <param name="tb"></param>
        private void changeDataDisplayed(ToolStripTextBox tb)
        {
            try
            {
                if (!tstb_SearchTaskNewProject.Text.Equals("") && !tstb_SearchTaskNewProject.Text.Equals("Cerca..."))
                {
                    selectCondition = Utility.getSQLCondition_ForeachColumn("ToDoList", tstb_SearchTaskNewProject.Text, " Reparto = 'Nuovo Progetto'");
                    dgv_taskNewProject.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] " + selectCondition + "AND Reparto = 'Nuovo Progetto' AND Stato <> 'Completato' ORDER BY Priorità DESC");
                }
                if (!tstb_SearchTaskRepMeccanico.Text.Equals("") && !tstb_SearchTaskRepMeccanico.Text.Equals("Cerca..."))
                {
                    selectCondition = Utility.getSQLCondition_ForeachColumn("ToDoList", tstb_SearchTaskRepMeccanico.Text, " Reparto = 'Meccanico'");
                    dgv_taskRepMeccanico.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] " + selectCondition + "AND Reparto = 'Meccanico' AND Stato <> 'Completato' ORDER BY Priorità DESC");
                }
                if (!tstb_SearchTaskRepElettrico.Text.Equals("") && !tstb_SearchTaskRepElettrico.Text.Equals("Cerca..."))
                {
                    selectCondition = Utility.getSQLCondition_ForeachColumn("ToDoList", tstb_SearchTaskRepElettrico.Text, " Reparto = 'Elettrico'");
                    dgv_taskRepElettrico.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] " + selectCondition + "AND Reparto = 'Elettrico' AND Stato <> 'Completato' ORDER BY Priorità DESC");
                }
                if (!tstb_SearchTaskRepSoftware.Text.Equals("") && !tstb_SearchTaskRepSoftware.Text.Equals("Cerca..."))
                {
                    selectCondition = Utility.getSQLCondition_ForeachColumn("ToDoList", tstb_SearchTaskRepSoftware.Text, " Reparto = 'Software'");
                    dgv_taskRepSoftware.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] " + selectCondition + " AND Reparto = 'Software' AND Stato <> 'Completato' ORDER BY Priorità DESC");
                }
                if (!tstb_SearchTaskRepQualita.Text.Equals("") && !tstb_SearchTaskRepQualita.Text.Equals("Cerca..."))
                {
                    selectCondition = Utility.getSQLCondition_ForeachColumn("ToDoList", tstb_SearchTaskRepQualita.Text, " Reparto = 'Qualita'");
                    dgv_taskRepQualita.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] " + selectCondition + "AND Reparto = 'Qualita' AND Stato <> 'Completato' ORDER BY Priorità DESC");
                }
                if (!tstb_SearchTaskCompleted.Text.Equals("") && !tstb_SearchTaskCompleted.Text.Equals("Cerca..."))
                {
                    selectCondition = Utility.getSQLCondition_ForeachColumn("ToDoList", tstb_SearchTaskCompleted.Text, " Stato = 'Completato'");
                    dgv_taskCompleted.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] " + selectCondition + " AND Stato = 'Completato' ORDER BY Priorità DESC");
                }
                if (!tstb_SearchTaskAll.Text.Equals("") && !tstb_SearchTaskAll.Text.Equals("Cerca..."))
                {
                    selectCondition = Utility.getSQLCondition_ForeachColumn("ToDoList", tstb_SearchTaskAll.Text, null);
                    dgv_taskAll.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] " + selectCondition + " ORDER BY Priorità DESC");
                }
                if (!tstb_SearchNotePersonali.Text.Equals("") && !tstb_SearchNotePersonali.Text.Equals("Cerca..."))
                {
                    selectCondition = Utility.getSQLCondition_ForeachColumn("ToDoList", tstb_SearchNotePersonali.Text, " Reparto = 'Note Personali'");
                    dgv_NotePersonali.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] " + selectCondition + "AND Reparto = 'Note Personali' AND [Aggiornato Da] = '" + user.Nome + " " + user.Cognome + "' ORDER BY Priorità DESC");
                }
                //aggiungere le atre ricerche QUI...
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setDataGridViewLayout()
        {
            try
            {
                //Console.WriteLine("TAB ATTIVO = " + tabControlList.SelectedTab.Tag.ToString());
                switch (tabControlList.SelectedTab.Tag.ToString())
                {
                    case "Nuovo Progetto":
                        {
                            dgv_taskNewProject.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] WHERE Reparto = 'Nuovo Progetto' AND Stato <> 'Completato' ORDER BY Priorità DESC ");
                            dgv_taskNewProject.Columns["ID"].Visible = false;
                            dgv_taskNewProject.Columns["Reparto"].Visible = false;
                            dgv_taskNewProject.Columns["Data Aggiornamento"].Visible = false;
                            dgv_taskNewProject.Columns["Inizio Lavoro"].Visible = false;
                            dgv_taskNewProject.Columns["Fine Lavoro"].Visible = false;
                            dgv_taskNewProject.Columns["Task"].Width = 600;
                            colorDataGridViewRows(dgv_taskNewProject);
                        }
                        break;
                    case "Meccanico":
                        {
                            dgv_taskRepMeccanico.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] WHERE Reparto = 'Meccanico' AND Stato <> 'Completato' ORDER BY Priorità DESC ");
                            dgv_taskRepMeccanico.Columns["ID"].Visible = false;
                            dgv_taskRepMeccanico.Columns["Reparto"].Visible = false;
                            dgv_taskRepMeccanico.Columns["Data Aggiornamento"].Visible = false;
                            dgv_taskRepMeccanico.Columns["Inizio Lavoro"].Visible = false;
                            dgv_taskRepMeccanico.Columns["Fine Lavoro"].Visible = false;
                            dgv_taskRepMeccanico.Columns["Task"].Width = 600;
                            colorDataGridViewRows(dgv_taskRepMeccanico);
                        }
                        break;
                    case "Elettrico":
                        {
                            dgv_taskRepElettrico.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] WHERE Reparto = 'Elettrico' AND Stato <> 'Completato' ORDER BY Priorità DESC ");
                            dgv_taskRepElettrico.Columns["ID"].Visible = false;
                            dgv_taskRepElettrico.Columns["Reparto"].Visible = false;
                            dgv_taskRepElettrico.Columns["Data Aggiornamento"].Visible = false;
                            dgv_taskRepElettrico.Columns["Inizio Lavoro"].Visible = false;
                            dgv_taskRepElettrico.Columns["Fine Lavoro"].Visible = false;
                            dgv_taskRepElettrico.Columns["Task"].Width = 600;
                            colorDataGridViewRows(dgv_taskRepElettrico);
                        }
                        break;
                    case "Software":
                        {
                            dgv_taskRepSoftware.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] WHERE Reparto = 'Software' AND Stato <> 'Completato' ORDER BY Priorità DESC");
                            dgv_taskRepSoftware.Columns["ID"].Visible = false;
                            dgv_taskRepSoftware.Columns["Reparto"].Visible = false;
                            dgv_taskRepSoftware.Columns["Data Aggiornamento"].Visible = false;
                            dgv_taskRepSoftware.Columns["Inizio Lavoro"].Visible = false;
                            dgv_taskRepSoftware.Columns["Fine Lavoro"].Visible = false;
                            dgv_taskRepSoftware.Columns["Task"].Width = 600;
                            colorDataGridViewRows(dgv_taskRepSoftware);
                        }
                        break;
                    case "Qualita":
                        {
                            dgv_taskRepQualita.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] WHERE Reparto = 'Qualita' AND Stato <> 'Completato' ORDER BY Priorità DESC ");
                            dgv_taskRepQualita.Columns["ID"].Visible = false;
                            dgv_taskRepQualita.Columns["Reparto"].Visible = false;
                            dgv_taskRepQualita.Columns["Data Aggiornamento"].Visible = false;
                            dgv_taskRepQualita.Columns["Inizio Lavoro"].Visible = false;
                            dgv_taskRepQualita.Columns["Fine Lavoro"].Visible = false;
                            dgv_taskRepQualita.Columns["Task"].Width = 600;
                            colorDataGridViewRows(dgv_taskRepQualita);
                        }
                        break;
                    case "Completati":
                        {
                            dgv_taskCompleted.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] WHERE Stato = 'Completato' ORDER BY Priorità DESC ");
                            dgv_taskCompleted.Columns["ID"].Visible = false;
                            dgv_taskCompleted.Columns["Task"].Width = 400;
                            dgv_taskCompleted.Columns["Inizio Lavoro"].Visible = false;
                            dgv_taskCompleted.Columns["Fine Lavoro"].Visible = false;
                            colorDataGridViewRows(dgv_taskCompleted);
                        }
                        break;
                    case "Overview":
                        {
                            dgv_taskAll.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] ORDER BY Priorità DESC ");
                            dgv_taskAll.Columns["ID"].Visible = false;
                            dgv_taskAll.Columns["Task"].Width = 400;
                            dgv_taskAll.Columns["Inizio Lavoro"].Visible = false;
                            dgv_taskAll.Columns["Fine Lavoro"].Visible = false;
                            colorDataGridViewRows(dgv_taskAll);
                        }
                        break;
                    case "Note Personali":
                        {
                            dgv_NotePersonali.DataSource = Utility.aggiornaDataGridView("SELECT * FROM [ToDoList] WHERE Reparto = 'Note Personali' AND [Aggiornato Da] = '" + user.Nome + " " + user.Cognome + "' ORDER BY Priorità DESC");
                            dgv_NotePersonali.Columns["ID"].Visible = false;
                            dgv_NotePersonali.Columns["Reparto"].Visible = false;
                            dgv_NotePersonali.Columns["Data Aggiornamento"].Visible = false;
                            dgv_NotePersonali.Columns["Inizio Lavoro"].Visible = false;
                            dgv_NotePersonali.Columns["Fine Lavoro"].Visible = false;
                            dgv_NotePersonali.Columns["Task"].Width = 600;
                            colorDataGridViewRows(dgv_NotePersonali);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void colorDataGridViewRows(DataGridView dgv)
        {
            try
            {
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    if (r.Cells["Priorità"].Value.ToString().Equals("3 - Top"))
                        r.DefaultCellStyle.BackColor = Color.Salmon;
                    else if (r.Cells["Priorità"].Value.ToString().Equals("2 - Alta"))
                        r.DefaultCellStyle.BackColor = Color.Orange;
                    else if (r.Cells["Priorità"].Value.ToString().Equals("1 - Media"))
                        r.DefaultCellStyle.BackColor = Color.LightGreen;
                    else if (r.Cells["Priorità"].Value.ToString().Equals("0 - Bassa"))
                        r.DefaultCellStyle.BackColor = Color.White;
                }
                dgv.DefaultCellStyle.Font = new Font("Arial", 12.0F, GraphicsUnit.Pixel);
                dgv.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Updates_Click(object sender, EventArgs e)
        {
            try
            {
                string pathServer = ConfigurationManager.AppSettings["SERVER_UPDATE_APP_PATH"];
                FileInfo serverFileInfo;
                FileInfo localFileInfo;
                string[] pathFiles = Directory.GetFiles(pathServer);
                if (pathServer != null)
                {
                    for (int i = 0; i < pathFiles.Length; i++)
                    {
                        string fileName = Path.GetFileName(pathFiles[i]);
                        serverFileInfo = new FileInfo(pathFiles[i]);
                        localFileInfo = new FileInfo(Environment.CurrentDirectory + @"\" + fileName);
                        if (!fileName.Equals("ToDoList.exe"))
                        {
                            if (serverFileInfo.LastWriteTime > localFileInfo.LastWriteTime)
                                File.Copy(serverFileInfo.ToString(), localFileInfo.ToString(), true);
                        }
                        else
                        {
                            File.Move(Environment.CurrentDirectory + @"\ToDoList.exe", Environment.CurrentDirectory + @"\ToDoList.bak");
                            File.Copy(serverFileInfo.ToString(), localFileInfo.ToString(), true);
                        }
                    }
                    DialogResult result = MessageBox.Show("Il programma verrà riavviato per consentire l\'installazione degli aggiornamenti.\nPremere OK per continuare...", "Informazione", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (result.Equals(DialogResult.OK))
                    {
                        //File.Delete(Environment.CurrentDirectory + @"\Ralco_ServiceManager.bak");
                        this.Dispose();
                        Application.Restart();
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region operazioniComuni

        private void addTask()
        {
            try
            {
                Form_Record formRec = new Form_Record(user, tabControlList.SelectedTab.Tag.ToString());
                var dialogResult = formRec.ShowDialog();
                if (dialogResult.Equals(DialogResult.OK))
                    tabControlList_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void editTask(DataGridViewCellEventArgs e, DataGridView dgv)
        {
            try
            {
                if (e.RowIndex != -1 && dgv.Rows[dgv.CurrentCell.RowIndex].Cells["ID"].Value.ToString() != "" && !dgv.SelectedCells.Count.Equals(dgv.Rows[dgv.CurrentCell.RowIndex].Cells.Count))
                {
                    int indexRecordToEdit = Convert.ToInt32(dgv.Rows[dgv.CurrentCell.RowIndex].Cells["ID"].Value.ToString());
                    Form_Record formRec = new Form_Record(user, indexRecordToEdit, false, false);
                    var dialogResult = formRec.ShowDialog();
                    if (dialogResult.Equals(DialogResult.OK))
                        tabControlList_SelectedIndexChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void deleteTask(DataGridView dgv)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("L'elemento selezionato verrà rimosso dal database.\nSei sicuro di voler procedere?", "Elimina Task", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    int actualIndex = dgv.CurrentCell.RowIndex;
                    Utility.executeQuery("DELETE FROM [ToDoList] WHERE " +
                           "[Id] = '" + dgv.Rows[actualIndex].Cells["Id"].Value.ToString() + "'");
                    tstb_SearchTaskRepSoftware.Text = "";
                    tabControlList_SelectedIndexChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void exportTask(DataGridView dgv)
        {
            try
            {
                Utility.saveAndVisualizeExcelReport(dgv, "Report " + tabControlList.SelectedTab.Tag.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region NewProject

        private void dgv_taskNewProject_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            editTask(e, dgv_taskNewProject);
        }

        private void tstb_SearchTaskNewProject_Click(object sender, EventArgs e)
        {
            tstb_SearchTaskNewProject.Text = "";
            tstb_SearchTaskNewProject_TextChanged(null, null);
        }

        private void tstb_SearchTaskNewProject_Leave(object sender, EventArgs e)
        {
            resetSearchTextboxAndDatagridviews(tstb_SearchTaskNewProject);
        }

        private void tstb_SearchTaskNewProject_TextChanged(object sender, EventArgs e)
        {
            changeDataDisplayed(tstb_SearchTaskNewProject);
        }

        private void btn_addTaskNewProject_Click(object sender, EventArgs e)
        {
            addTask();
        }

        private void btn_delTaskNewProject_Click(object sender, EventArgs e)
        {
            deleteTask(dgv_taskNewProject);
        }

        private void btn_exportTaskNewProject_Click(object sender, EventArgs e)
        {
            exportTask(dgv_taskNewProject);
        }

        private void dgv_taskNewProject_Sorted(object sender, EventArgs e)
        {
            colorDataGridViewRows(dgv_taskNewProject);
        }

        #endregion

        #region taskMeccanico

        private void dgv_taskRepMeccanico_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            editTask(e, dgv_taskRepMeccanico);
        }

        private void tstb_SearchTaskRepMeccanico_Click(object sender, EventArgs e)
        {
            tstb_SearchTaskRepMeccanico.Text = "";
            tstb_SearchTaskRepMeccanico_TextChanged(null, null);
        }

        private void tstb_SearchTaskRepMeccanico_Leave(object sender, EventArgs e)
        {
            resetSearchTextboxAndDatagridviews(tstb_SearchTaskRepMeccanico);
        }

        private void tstb_SearchTaskRepMeccanico_TextChanged(object sender, EventArgs e)
        {
            changeDataDisplayed(tstb_SearchTaskRepMeccanico);
        }

        private void btn_addTaskRepMeccanico_Click(object sender, EventArgs e)
        {
            addTask();
        }

        private void btn_delTaskRepMeccanico_Click(object sender, EventArgs e)
        {
            deleteTask(dgv_taskRepMeccanico);
        }

        private void btn_exportTaskRepMeccanico_Click(object sender, EventArgs e)
        {
            exportTask(dgv_taskRepMeccanico);
        }

        private void dgv_taskRepMeccanico_Sorted(object sender, EventArgs e)
        {
            colorDataGridViewRows(dgv_taskRepMeccanico);
        }

        #endregion

        #region taskElettrico

        private void dgv_taskRepElettrico_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            editTask(e, dgv_taskRepElettrico);
        }

        private void tstb_SearchTaskRepElettrico_Click(object sender, EventArgs e)
        {
            tstb_SearchTaskRepElettrico.Text = "";
            tstb_SearchTaskRepElettrico_TextChanged(null, null);
        }

        private void tstb_SearchTaskRepElettrico_Leave(object sender, EventArgs e)
        {
            resetSearchTextboxAndDatagridviews(tstb_SearchTaskRepElettrico);
        }

        private void tstb_SearchTaskRepElettrico_TextChanged(object sender, EventArgs e)
        {
            changeDataDisplayed(tstb_SearchTaskRepElettrico);
        }

        private void btn_addTaskRepElettrico_Click(object sender, EventArgs e)
        {
            addTask();
        }

        private void btn_delTaskRepElettrico_Click(object sender, EventArgs e)
        {
            deleteTask(dgv_taskRepElettrico);
        }

        private void btn_exportTaskRepElettrico_Click(object sender, EventArgs e)
        {
            exportTask(dgv_taskRepElettrico);
        }

        private void dgv_taskRepElettrico_Sorted(object sender, EventArgs e)
        {
            colorDataGridViewRows(dgv_taskRepElettrico);
        }

        #endregion

        #region taskSoftware

        private void dgv_taskRepSoftware_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            editTask(e, dgv_taskRepSoftware);
        }

        private void tstb_SearchTaskRepSoftware_Click(object sender, EventArgs e)
        {
            tstb_SearchTaskRepSoftware.Text = "";
            tstb_SearchTaskRepSoftware_TextChanged(null, null);
        }

        private void tstb_SearchTaskRepSoftware_Leave(object sender, EventArgs e)
        {
            resetSearchTextboxAndDatagridviews(tstb_SearchTaskRepSoftware);
        }

        private void tstb_SearchTaskRepSoftware_TextChanged(object sender, EventArgs e)
        {
            changeDataDisplayed(tstb_SearchTaskRepSoftware);
        }

        private void btn_addTaskRepSoftware_Click(object sender, EventArgs e)
        {
            addTask();
        }

        private void btn_delTaskRepSoftware_Click(object sender, EventArgs e)
        {
            deleteTask(dgv_taskRepSoftware);
        }

        private void btn_exportTaskRepSoftware_Click(object sender, EventArgs e)
        {
            exportTask(dgv_taskRepSoftware);
        }

        private void dgv_taskRepSoftware_Sorted(object sender, EventArgs e)
        {
            colorDataGridViewRows(dgv_taskRepSoftware);
        }

        #endregion

        #region Qualità

        private void dgv_taskRepQualita_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            editTask(e, dgv_taskRepQualita);
        }

        private void tstb_SearchTaskRepQualita_Click(object sender, EventArgs e)
        {
            tstb_SearchTaskRepQualita.Text = "";
            tstb_SearchTaskRepQualita_TextChanged(null, null);
        }

        private void tstb_SearchTaskRepQualita_Leave(object sender, EventArgs e)
        {
            resetSearchTextboxAndDatagridviews(tstb_SearchTaskRepQualita);
        }

        private void tstb_SearchTaskRepQualita_TextChanged(object sender, EventArgs e)
        {
            changeDataDisplayed(tstb_SearchTaskRepQualita);
        }

        private void btn_addTaskRepQualita_Click(object sender, EventArgs e)
        {
            addTask();
        }

        private void btn_delTaskRepQualita_Click(object sender, EventArgs e)
        {
            deleteTask(dgv_taskRepQualita);
        }

        private void btn_exportTaskRepQualita_Click(object sender, EventArgs e)
        {
            exportTask(dgv_taskRepQualita);
        }

        private void dgv_taskRepQualita_Sorted(object sender, EventArgs e)
        {
            colorDataGridViewRows(dgv_taskRepQualita);
        }

        #endregion


        #region taskCompleted

        private void dgv_taskCompleted_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            editTask(e, dgv_taskCompleted);
        }

        private void tstb_SearchTaskCompleted_Click(object sender, EventArgs e)
        {
            tstb_SearchTaskCompleted.Text = "";
            tstb_SearchTaskCompleted_TextChanged(null, null);
        }

        private void tstb_SearchTaskCompleted_Leave(object sender, EventArgs e)
        {
            resetSearchTextboxAndDatagridviews(tstb_SearchTaskCompleted);
        }

        private void tstb_SearchTaskCompleted_TextChanged(object sender, EventArgs e)
        {
            changeDataDisplayed(tstb_SearchTaskCompleted);
        }

        private void btn_exportTaskCompleted_Click(object sender, EventArgs e)
        {
            exportTask(dgv_taskCompleted);
        }

        private void dgv_taskCompleted_Sorted(object sender, EventArgs e)
        {
            colorDataGridViewRows(dgv_taskCompleted);
        }

        #endregion


        #region taskOverview

        private void dgv_taskAll_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            editTask(e, dgv_taskAll);
        }

        private void tstb_SearchTaskAll_Click(object sender, EventArgs e)
        {
            tstb_SearchTaskAll.Text = "";
            tstb_SearchTaskAll_TextChanged(null, null);
        }

        private void tstb_SearchTaskAll_Leave(object sender, EventArgs e)
        {
            resetSearchTextboxAndDatagridviews(tstb_SearchTaskAll);
        }

        private void tstb_SearchTaskAll_TextChanged(object sender, EventArgs e)
        {
            changeDataDisplayed(tstb_SearchTaskAll);
        }

        private void btn_addTaskAll_Click(object sender, EventArgs e)
        {
            addTask();
        }

        private void btn_delTaskAll_Click(object sender, EventArgs e)
        {
            deleteTask(dgv_taskAll);
        }

        private void btn_exportTaskAll_Click(object sender, EventArgs e)
        {
            exportTask(dgv_taskAll);
        }

        private void dgv_taskAll_Sorted(object sender, EventArgs e)
        {
            colorDataGridViewRows(dgv_taskAll);
        }

        #endregion

        #region Note Personali

        private void dgv_NotePersonali_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            editTask(e, dgv_NotePersonali);
        }
        private void tstb_SearchNotePersonali_Click(object sender, EventArgs e)
        {
            tstb_SearchNotePersonali.Text = "";
            tstb_SearchNotePersonali_TextChanged(null, null);
        }

        private void tstb_SearchNotePersonali_Leave(object sender, EventArgs e)
        {
            resetSearchTextboxAndDatagridviews(tstb_SearchNotePersonali);
        }

        private void tstb_SearchNotePersonali_TextChanged(object sender, EventArgs e)
        {
            changeDataDisplayed(tstb_SearchNotePersonali);
        }


        private void btn_addNotePersonali_Click(object sender, EventArgs e)
        {
            addTask();
        }

        private void btn_delNotePersonali_Click(object sender, EventArgs e)
        {
            deleteTask(dgv_NotePersonali);
        }

        private void btn_exportNotePersonali_Click(object sender, EventArgs e)
        {
            exportTask(dgv_NotePersonali);
        }

        private void dgv_NotePersonali_Sorted(object sender, EventArgs e)
        {
            colorDataGridViewRows(dgv_NotePersonali);
        }

        #endregion

    }
}
