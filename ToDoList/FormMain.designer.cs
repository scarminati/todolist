﻿namespace ToDoList
{
    partial class Form_Main
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.tabControlList = new System.Windows.Forms.TabControl();
            this.tabNuoviProgetti = new System.Windows.Forms.TabPage();
            this.dgv_taskNewProject = new System.Windows.Forms.DataGridView();
            this.toolStrip8 = new System.Windows.Forms.ToolStrip();
            this.btn_addTaskNewProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton22 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator30 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton23 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator31 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_delTaskNewProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator32 = new System.Windows.Forms.ToolStripSeparator();
            this.tstb_SearchTaskNewProject = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator33 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_exportTaskNewProject = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton27 = new System.Windows.Forms.ToolStripButton();
            this.tabMeccanica = new System.Windows.Forms.TabPage();
            this.dgv_taskRepMeccanico = new System.Windows.Forms.DataGridView();
            this.toolStrip4 = new System.Windows.Forms.ToolStrip();
            this.btn_addTaskRepMeccanico = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton14 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton15 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_delTaskRepMeccanico = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.tstb_SearchTaskRepMeccanico = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_exportTaskRepMeccanico = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton18 = new System.Windows.Forms.ToolStripButton();
            this.tabElettrica = new System.Windows.Forms.TabPage();
            this.dgv_taskRepElettrico = new System.Windows.Forms.DataGridView();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.btn_addTaskRepElettrico = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_delTaskRepElettrico = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.tstb_SearchTaskRepElettrico = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_exportTaskRepElettrico = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton12 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.tabSoftware = new System.Windows.Forms.TabPage();
            this.dgv_taskRepSoftware = new System.Windows.Forms.DataGridView();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btn_addTaskRepSoftware = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.btn_delTaskRepSoftware = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tstb_SearchTaskRepSoftware = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_exportTaskRepSoftware = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.tabQualita = new System.Windows.Forms.TabPage();
            this.dgv_taskRepQualita = new System.Windows.Forms.DataGridView();
            this.toolStrip7 = new System.Windows.Forms.ToolStrip();
            this.btn_addTaskRepQualita = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton19 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator26 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton20 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator27 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_delTaskRepQualita = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator28 = new System.Windows.Forms.ToolStripSeparator();
            this.tstb_SearchTaskRepQualita = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator29 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_exportTaskRepQualita = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton24 = new System.Windows.Forms.ToolStripButton();
            this.tabCompletati = new System.Windows.Forms.TabPage();
            this.dgv_taskCompleted = new System.Windows.Forms.DataGridView();
            this.toolStrip6 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator22 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator23 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton11 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton13 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator24 = new System.Windows.Forms.ToolStripSeparator();
            this.tstb_SearchTaskCompleted = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator25 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_exportTaskCompleted = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton17 = new System.Windows.Forms.ToolStripButton();
            this.tabOverview = new System.Windows.Forms.TabPage();
            this.dgv_taskAll = new System.Windows.Forms.DataGridView();
            this.toolStrip5 = new System.Windows.Forms.ToolStrip();
            this.btn_addTaskAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.btn_delTaskAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.tstb_SearchTaskAll = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator21 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_exportTaskAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton21 = new System.Windows.Forms.ToolStripButton();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.lbl_NomeUtente = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_AddMagazzino = new System.Windows.Forms.ToolStripButton();
            this.btn_CloneMagazzino = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_ColorLines = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_RemoveMagazzino = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_ExportMagazzino = new System.Windows.Forms.ToolStripButton();
            this.btn_PrintTestFiltro = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btn_Updates = new System.Windows.Forms.ToolStripButton();
            this.timerTempoLavorato = new System.Windows.Forms.Timer(this.components);
            this.tabNotePersonali = new System.Windows.Forms.TabPage();
            this.toolStrip9 = new System.Windows.Forms.ToolStrip();
            this.btn_addNotePersonali = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton25 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator34 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton26 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator35 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_delNotePersonali = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator36 = new System.Windows.Forms.ToolStripSeparator();
            this.tstb_SearchNotePersonali = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator37 = new System.Windows.Forms.ToolStripSeparator();
            this.btn_exportNotePersonali = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton30 = new System.Windows.Forms.ToolStripButton();
            this.dgv_NotePersonali = new System.Windows.Forms.DataGridView();
            this.tabControlList.SuspendLayout();
            this.tabNuoviProgetti.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_taskNewProject)).BeginInit();
            this.toolStrip8.SuspendLayout();
            this.tabMeccanica.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_taskRepMeccanico)).BeginInit();
            this.toolStrip4.SuspendLayout();
            this.tabElettrica.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_taskRepElettrico)).BeginInit();
            this.toolStrip3.SuspendLayout();
            this.tabSoftware.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_taskRepSoftware)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.tabQualita.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_taskRepQualita)).BeginInit();
            this.toolStrip7.SuspendLayout();
            this.tabCompletati.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_taskCompleted)).BeginInit();
            this.toolStrip6.SuspendLayout();
            this.tabOverview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_taskAll)).BeginInit();
            this.toolStrip5.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tabNotePersonali.SuspendLayout();
            this.toolStrip9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_NotePersonali)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlList
            // 
            this.tabControlList.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControlList.Controls.Add(this.tabNuoviProgetti);
            this.tabControlList.Controls.Add(this.tabMeccanica);
            this.tabControlList.Controls.Add(this.tabElettrica);
            this.tabControlList.Controls.Add(this.tabSoftware);
            this.tabControlList.Controls.Add(this.tabQualita);
            this.tabControlList.Controls.Add(this.tabCompletati);
            this.tabControlList.Controls.Add(this.tabOverview);
            this.tabControlList.Controls.Add(this.tabNotePersonali);
            this.tabControlList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlList.ImageList = this.imageList;
            this.tabControlList.Location = new System.Drawing.Point(0, 50);
            this.tabControlList.Name = "tabControlList";
            this.tabControlList.SelectedIndex = 0;
            this.tabControlList.Size = new System.Drawing.Size(1242, 462);
            this.tabControlList.TabIndex = 8;
            this.tabControlList.SelectedIndexChanged += new System.EventHandler(this.tabControlList_SelectedIndexChanged);
            // 
            // tabNuoviProgetti
            // 
            this.tabNuoviProgetti.Controls.Add(this.dgv_taskNewProject);
            this.tabNuoviProgetti.Controls.Add(this.toolStrip8);
            this.tabNuoviProgetti.ImageIndex = 10;
            this.tabNuoviProgetti.Location = new System.Drawing.Point(4, 60);
            this.tabNuoviProgetti.Name = "tabNuoviProgetti";
            this.tabNuoviProgetti.Size = new System.Drawing.Size(1234, 398);
            this.tabNuoviProgetti.TabIndex = 7;
            this.tabNuoviProgetti.Tag = "Nuovo Progetto";
            this.tabNuoviProgetti.ToolTipText = "Nuovi Progetti";
            this.tabNuoviProgetti.UseVisualStyleBackColor = true;
            // 
            // dgv_taskNewProject
            // 
            this.dgv_taskNewProject.AllowUserToAddRows = false;
            this.dgv_taskNewProject.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_taskNewProject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_taskNewProject.Location = new System.Drawing.Point(0, 50);
            this.dgv_taskNewProject.Name = "dgv_taskNewProject";
            this.dgv_taskNewProject.ReadOnly = true;
            this.dgv_taskNewProject.Size = new System.Drawing.Size(1234, 348);
            this.dgv_taskNewProject.TabIndex = 9;
            this.dgv_taskNewProject.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_taskNewProject_CellClick);
            this.dgv_taskNewProject.Sorted += new System.EventHandler(this.dgv_taskNewProject_Sorted);
            // 
            // toolStrip8
            // 
            this.toolStrip8.AutoSize = false;
            this.toolStrip8.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip8.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip8.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip8.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_addTaskNewProject,
            this.toolStripButton22,
            this.toolStripSeparator30,
            this.toolStripButton23,
            this.toolStripSeparator31,
            this.btn_delTaskNewProject,
            this.toolStripSeparator32,
            this.tstb_SearchTaskNewProject,
            this.toolStripSeparator33,
            this.btn_exportTaskNewProject,
            this.toolStripButton27});
            this.toolStrip8.Location = new System.Drawing.Point(0, 0);
            this.toolStrip8.Name = "toolStrip8";
            this.toolStrip8.Size = new System.Drawing.Size(1234, 50);
            this.toolStrip8.TabIndex = 4;
            this.toolStrip8.Text = "toolStrip8";
            // 
            // btn_addTaskNewProject
            // 
            this.btn_addTaskNewProject.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_addTaskNewProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_addTaskNewProject.Image = ((System.Drawing.Image)(resources.GetObject("btn_addTaskNewProject.Image")));
            this.btn_addTaskNewProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_addTaskNewProject.Name = "btn_addTaskNewProject";
            this.btn_addTaskNewProject.Size = new System.Drawing.Size(51, 47);
            this.btn_addTaskNewProject.ToolTipText = "Aggiungi Task";
            this.btn_addTaskNewProject.Click += new System.EventHandler(this.btn_addTaskNewProject_Click);
            // 
            // toolStripButton22
            // 
            this.toolStripButton22.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton22.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton22.Image")));
            this.toolStripButton22.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton22.Name = "toolStripButton22";
            this.toolStripButton22.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton22.Text = "Clona Record";
            this.toolStripButton22.Visible = false;
            // 
            // toolStripSeparator30
            // 
            this.toolStripSeparator30.Name = "toolStripSeparator30";
            this.toolStripSeparator30.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator30.Visible = false;
            // 
            // toolStripButton23
            // 
            this.toolStripButton23.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton23.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton23.Image")));
            this.toolStripButton23.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton23.Name = "toolStripButton23";
            this.toolStripButton23.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton23.Text = "Evidenzia";
            this.toolStripButton23.ToolTipText = "Evidenzia";
            this.toolStripButton23.Visible = false;
            // 
            // toolStripSeparator31
            // 
            this.toolStripSeparator31.Name = "toolStripSeparator31";
            this.toolStripSeparator31.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_delTaskNewProject
            // 
            this.btn_delTaskNewProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_delTaskNewProject.Image = ((System.Drawing.Image)(resources.GetObject("btn_delTaskNewProject.Image")));
            this.btn_delTaskNewProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_delTaskNewProject.Name = "btn_delTaskNewProject";
            this.btn_delTaskNewProject.Size = new System.Drawing.Size(51, 47);
            this.btn_delTaskNewProject.Text = "toolStripButton2";
            this.btn_delTaskNewProject.ToolTipText = "Rimuovi Task";
            this.btn_delTaskNewProject.Click += new System.EventHandler(this.btn_delTaskNewProject_Click);
            // 
            // toolStripSeparator32
            // 
            this.toolStripSeparator32.Name = "toolStripSeparator32";
            this.toolStripSeparator32.Size = new System.Drawing.Size(6, 50);
            // 
            // tstb_SearchTaskNewProject
            // 
            this.tstb_SearchTaskNewProject.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tstb_SearchTaskNewProject.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tstb_SearchTaskNewProject.Name = "tstb_SearchTaskNewProject";
            this.tstb_SearchTaskNewProject.Size = new System.Drawing.Size(100, 50);
            this.tstb_SearchTaskNewProject.Text = "Cerca...";
            this.tstb_SearchTaskNewProject.ToolTipText = "Cerca Task";
            this.tstb_SearchTaskNewProject.Leave += new System.EventHandler(this.tstb_SearchTaskNewProject_Leave);
            this.tstb_SearchTaskNewProject.Click += new System.EventHandler(this.tstb_SearchTaskNewProject_Click);
            this.tstb_SearchTaskNewProject.TextChanged += new System.EventHandler(this.tstb_SearchTaskNewProject_TextChanged);
            // 
            // toolStripSeparator33
            // 
            this.toolStripSeparator33.Name = "toolStripSeparator33";
            this.toolStripSeparator33.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_exportTaskNewProject
            // 
            this.btn_exportTaskNewProject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_exportTaskNewProject.Image = ((System.Drawing.Image)(resources.GetObject("btn_exportTaskNewProject.Image")));
            this.btn_exportTaskNewProject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_exportTaskNewProject.Name = "btn_exportTaskNewProject";
            this.btn_exportTaskNewProject.Size = new System.Drawing.Size(51, 47);
            this.btn_exportTaskNewProject.Text = "toolStripButton4";
            this.btn_exportTaskNewProject.ToolTipText = "Esporta Lista Task";
            this.btn_exportTaskNewProject.Click += new System.EventHandler(this.btn_exportTaskNewProject_Click);
            // 
            // toolStripButton27
            // 
            this.toolStripButton27.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton27.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton27.Image")));
            this.toolStripButton27.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton27.Name = "toolStripButton27";
            this.toolStripButton27.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton27.ToolTipText = "Stampa Test";
            this.toolStripButton27.Visible = false;
            // 
            // tabMeccanica
            // 
            this.tabMeccanica.Controls.Add(this.dgv_taskRepMeccanico);
            this.tabMeccanica.Controls.Add(this.toolStrip4);
            this.tabMeccanica.ImageIndex = 5;
            this.tabMeccanica.Location = new System.Drawing.Point(4, 60);
            this.tabMeccanica.Name = "tabMeccanica";
            this.tabMeccanica.Size = new System.Drawing.Size(1234, 398);
            this.tabMeccanica.TabIndex = 2;
            this.tabMeccanica.Tag = "Meccanico";
            this.tabMeccanica.ToolTipText = "Meccanica";
            this.tabMeccanica.UseVisualStyleBackColor = true;
            // 
            // dgv_taskRepMeccanico
            // 
            this.dgv_taskRepMeccanico.AllowUserToAddRows = false;
            this.dgv_taskRepMeccanico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_taskRepMeccanico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_taskRepMeccanico.Location = new System.Drawing.Point(0, 50);
            this.dgv_taskRepMeccanico.Name = "dgv_taskRepMeccanico";
            this.dgv_taskRepMeccanico.ReadOnly = true;
            this.dgv_taskRepMeccanico.Size = new System.Drawing.Size(1234, 348);
            this.dgv_taskRepMeccanico.TabIndex = 8;
            this.dgv_taskRepMeccanico.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_taskRepMeccanico_CellClick);
            this.dgv_taskRepMeccanico.Sorted += new System.EventHandler(this.dgv_taskRepMeccanico_Sorted);
            // 
            // toolStrip4
            // 
            this.toolStrip4.AutoSize = false;
            this.toolStrip4.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip4.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_addTaskRepMeccanico,
            this.toolStripButton14,
            this.toolStripSeparator15,
            this.toolStripButton15,
            this.toolStripSeparator16,
            this.btn_delTaskRepMeccanico,
            this.toolStripSeparator17,
            this.tstb_SearchTaskRepMeccanico,
            this.toolStripSeparator18,
            this.btn_exportTaskRepMeccanico,
            this.toolStripButton18});
            this.toolStrip4.Location = new System.Drawing.Point(0, 0);
            this.toolStrip4.Name = "toolStrip4";
            this.toolStrip4.Size = new System.Drawing.Size(1234, 50);
            this.toolStrip4.TabIndex = 3;
            this.toolStrip4.Text = "toolStrip4";
            // 
            // btn_addTaskRepMeccanico
            // 
            this.btn_addTaskRepMeccanico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_addTaskRepMeccanico.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_addTaskRepMeccanico.Image = ((System.Drawing.Image)(resources.GetObject("btn_addTaskRepMeccanico.Image")));
            this.btn_addTaskRepMeccanico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_addTaskRepMeccanico.Name = "btn_addTaskRepMeccanico";
            this.btn_addTaskRepMeccanico.Size = new System.Drawing.Size(51, 47);
            this.btn_addTaskRepMeccanico.ToolTipText = "Aggiungi Task";
            this.btn_addTaskRepMeccanico.Click += new System.EventHandler(this.btn_addTaskRepMeccanico_Click);
            // 
            // toolStripButton14
            // 
            this.toolStripButton14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton14.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton14.Image")));
            this.toolStripButton14.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton14.Name = "toolStripButton14";
            this.toolStripButton14.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton14.Text = "Clona Record";
            this.toolStripButton14.Visible = false;
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator15.Visible = false;
            // 
            // toolStripButton15
            // 
            this.toolStripButton15.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton15.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton15.Image")));
            this.toolStripButton15.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton15.Name = "toolStripButton15";
            this.toolStripButton15.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton15.Text = "Evidenzia";
            this.toolStripButton15.ToolTipText = "Evidenzia";
            this.toolStripButton15.Visible = false;
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_delTaskRepMeccanico
            // 
            this.btn_delTaskRepMeccanico.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_delTaskRepMeccanico.Image = ((System.Drawing.Image)(resources.GetObject("btn_delTaskRepMeccanico.Image")));
            this.btn_delTaskRepMeccanico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_delTaskRepMeccanico.Name = "btn_delTaskRepMeccanico";
            this.btn_delTaskRepMeccanico.Size = new System.Drawing.Size(51, 47);
            this.btn_delTaskRepMeccanico.Text = "toolStripButton2";
            this.btn_delTaskRepMeccanico.ToolTipText = "Rimuovi Task";
            this.btn_delTaskRepMeccanico.Click += new System.EventHandler(this.btn_delTaskRepMeccanico_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(6, 50);
            // 
            // tstb_SearchTaskRepMeccanico
            // 
            this.tstb_SearchTaskRepMeccanico.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tstb_SearchTaskRepMeccanico.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tstb_SearchTaskRepMeccanico.Name = "tstb_SearchTaskRepMeccanico";
            this.tstb_SearchTaskRepMeccanico.Size = new System.Drawing.Size(100, 50);
            this.tstb_SearchTaskRepMeccanico.Text = "Cerca...";
            this.tstb_SearchTaskRepMeccanico.ToolTipText = "Cerca Task";
            this.tstb_SearchTaskRepMeccanico.Leave += new System.EventHandler(this.tstb_SearchTaskRepMeccanico_Leave);
            this.tstb_SearchTaskRepMeccanico.Click += new System.EventHandler(this.tstb_SearchTaskRepMeccanico_Click);
            this.tstb_SearchTaskRepMeccanico.TextChanged += new System.EventHandler(this.tstb_SearchTaskRepMeccanico_TextChanged);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_exportTaskRepMeccanico
            // 
            this.btn_exportTaskRepMeccanico.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_exportTaskRepMeccanico.Image = ((System.Drawing.Image)(resources.GetObject("btn_exportTaskRepMeccanico.Image")));
            this.btn_exportTaskRepMeccanico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_exportTaskRepMeccanico.Name = "btn_exportTaskRepMeccanico";
            this.btn_exportTaskRepMeccanico.Size = new System.Drawing.Size(51, 47);
            this.btn_exportTaskRepMeccanico.Text = "toolStripButton4";
            this.btn_exportTaskRepMeccanico.ToolTipText = "Esporta Lista Task";
            this.btn_exportTaskRepMeccanico.Click += new System.EventHandler(this.btn_exportTaskRepMeccanico_Click);
            // 
            // toolStripButton18
            // 
            this.toolStripButton18.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton18.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton18.Image")));
            this.toolStripButton18.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton18.Name = "toolStripButton18";
            this.toolStripButton18.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton18.ToolTipText = "Stampa Test";
            this.toolStripButton18.Visible = false;
            // 
            // tabElettrica
            // 
            this.tabElettrica.Controls.Add(this.dgv_taskRepElettrico);
            this.tabElettrica.Controls.Add(this.toolStrip3);
            this.tabElettrica.ImageIndex = 0;
            this.tabElettrica.Location = new System.Drawing.Point(4, 60);
            this.tabElettrica.Name = "tabElettrica";
            this.tabElettrica.Size = new System.Drawing.Size(1234, 398);
            this.tabElettrica.TabIndex = 1;
            this.tabElettrica.Tag = "Elettrico";
            this.tabElettrica.ToolTipText = "Elettrica";
            this.tabElettrica.UseVisualStyleBackColor = true;
            // 
            // dgv_taskRepElettrico
            // 
            this.dgv_taskRepElettrico.AllowUserToAddRows = false;
            this.dgv_taskRepElettrico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_taskRepElettrico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_taskRepElettrico.Location = new System.Drawing.Point(0, 50);
            this.dgv_taskRepElettrico.Name = "dgv_taskRepElettrico";
            this.dgv_taskRepElettrico.ReadOnly = true;
            this.dgv_taskRepElettrico.Size = new System.Drawing.Size(1234, 348);
            this.dgv_taskRepElettrico.TabIndex = 8;
            this.dgv_taskRepElettrico.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_taskRepElettrico_CellClick);
            this.dgv_taskRepElettrico.Sorted += new System.EventHandler(this.dgv_taskRepElettrico_Sorted);
            // 
            // toolStrip3
            // 
            this.toolStrip3.AutoSize = false;
            this.toolStrip3.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip3.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_addTaskRepElettrico,
            this.toolStripButton8,
            this.toolStripSeparator4,
            this.toolStripButton9,
            this.toolStripSeparator5,
            this.btn_delTaskRepElettrico,
            this.toolStripSeparator13,
            this.tstb_SearchTaskRepElettrico,
            this.toolStripSeparator14,
            this.btn_exportTaskRepElettrico,
            this.toolStripButton12,
            this.toolStripButton1});
            this.toolStrip3.Location = new System.Drawing.Point(0, 0);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(1234, 50);
            this.toolStrip3.TabIndex = 3;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // btn_addTaskRepElettrico
            // 
            this.btn_addTaskRepElettrico.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_addTaskRepElettrico.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_addTaskRepElettrico.Image = ((System.Drawing.Image)(resources.GetObject("btn_addTaskRepElettrico.Image")));
            this.btn_addTaskRepElettrico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_addTaskRepElettrico.Name = "btn_addTaskRepElettrico";
            this.btn_addTaskRepElettrico.Size = new System.Drawing.Size(51, 47);
            this.btn_addTaskRepElettrico.ToolTipText = "Aggiungi Task";
            this.btn_addTaskRepElettrico.Click += new System.EventHandler(this.btn_addTaskRepElettrico_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton8.Image")));
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton8.Text = "Clona Record";
            this.toolStripButton8.Visible = false;
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator4.Visible = false;
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton9.Image")));
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton9.Text = "Evidenzia";
            this.toolStripButton9.ToolTipText = "Evidenzia";
            this.toolStripButton9.Visible = false;
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_delTaskRepElettrico
            // 
            this.btn_delTaskRepElettrico.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_delTaskRepElettrico.Image = ((System.Drawing.Image)(resources.GetObject("btn_delTaskRepElettrico.Image")));
            this.btn_delTaskRepElettrico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_delTaskRepElettrico.Name = "btn_delTaskRepElettrico";
            this.btn_delTaskRepElettrico.Size = new System.Drawing.Size(51, 47);
            this.btn_delTaskRepElettrico.Text = "toolStripButton2";
            this.btn_delTaskRepElettrico.ToolTipText = "Rimuovi Task";
            this.btn_delTaskRepElettrico.Click += new System.EventHandler(this.btn_delTaskRepElettrico_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(6, 50);
            // 
            // tstb_SearchTaskRepElettrico
            // 
            this.tstb_SearchTaskRepElettrico.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tstb_SearchTaskRepElettrico.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tstb_SearchTaskRepElettrico.Name = "tstb_SearchTaskRepElettrico";
            this.tstb_SearchTaskRepElettrico.Size = new System.Drawing.Size(100, 50);
            this.tstb_SearchTaskRepElettrico.Text = "Cerca...";
            this.tstb_SearchTaskRepElettrico.ToolTipText = "Cerca Task";
            this.tstb_SearchTaskRepElettrico.Leave += new System.EventHandler(this.tstb_SearchTaskRepElettrico_Leave);
            this.tstb_SearchTaskRepElettrico.Click += new System.EventHandler(this.tstb_SearchTaskRepElettrico_Click);
            this.tstb_SearchTaskRepElettrico.TextChanged += new System.EventHandler(this.tstb_SearchTaskRepElettrico_TextChanged);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_exportTaskRepElettrico
            // 
            this.btn_exportTaskRepElettrico.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_exportTaskRepElettrico.Image = ((System.Drawing.Image)(resources.GetObject("btn_exportTaskRepElettrico.Image")));
            this.btn_exportTaskRepElettrico.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_exportTaskRepElettrico.Name = "btn_exportTaskRepElettrico";
            this.btn_exportTaskRepElettrico.Size = new System.Drawing.Size(51, 47);
            this.btn_exportTaskRepElettrico.Text = "toolStripButton4";
            this.btn_exportTaskRepElettrico.ToolTipText = "Esporta Lista Task";
            this.btn_exportTaskRepElettrico.Click += new System.EventHandler(this.btn_exportTaskRepElettrico_Click);
            // 
            // toolStripButton12
            // 
            this.toolStripButton12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton12.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton12.Image")));
            this.toolStripButton12.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton12.Name = "toolStripButton12";
            this.toolStripButton12.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton12.ToolTipText = "Stampa Test";
            this.toolStripButton12.Visible = false;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 47);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // tabSoftware
            // 
            this.tabSoftware.Controls.Add(this.dgv_taskRepSoftware);
            this.tabSoftware.Controls.Add(this.toolStrip2);
            this.tabSoftware.ImageIndex = 6;
            this.tabSoftware.Location = new System.Drawing.Point(4, 60);
            this.tabSoftware.Name = "tabSoftware";
            this.tabSoftware.Size = new System.Drawing.Size(1234, 398);
            this.tabSoftware.TabIndex = 3;
            this.tabSoftware.Tag = "Software";
            this.tabSoftware.ToolTipText = "Software";
            this.tabSoftware.UseVisualStyleBackColor = true;
            // 
            // dgv_taskRepSoftware
            // 
            this.dgv_taskRepSoftware.AllowUserToAddRows = false;
            this.dgv_taskRepSoftware.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_taskRepSoftware.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_taskRepSoftware.Location = new System.Drawing.Point(0, 50);
            this.dgv_taskRepSoftware.Name = "dgv_taskRepSoftware";
            this.dgv_taskRepSoftware.ReadOnly = true;
            this.dgv_taskRepSoftware.Size = new System.Drawing.Size(1234, 348);
            this.dgv_taskRepSoftware.TabIndex = 9;
            this.dgv_taskRepSoftware.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_taskRepSoftware_CellClick);
            this.dgv_taskRepSoftware.Sorted += new System.EventHandler(this.dgv_taskRepSoftware_Sorted);
            // 
            // toolStrip2
            // 
            this.toolStrip2.AutoSize = false;
            this.toolStrip2.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_addTaskRepSoftware,
            this.toolStripSeparator7,
            this.toolStripButton2,
            this.toolStripSeparator8,
            this.toolStripButton3,
            this.btn_delTaskRepSoftware,
            this.toolStripSeparator6,
            this.tstb_SearchTaskRepSoftware,
            this.toolStripSeparator9,
            this.btn_exportTaskRepSoftware,
            this.toolStripButton6});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(1234, 50);
            this.toolStrip2.TabIndex = 4;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // btn_addTaskRepSoftware
            // 
            this.btn_addTaskRepSoftware.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_addTaskRepSoftware.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_addTaskRepSoftware.Image = ((System.Drawing.Image)(resources.GetObject("btn_addTaskRepSoftware.Image")));
            this.btn_addTaskRepSoftware.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_addTaskRepSoftware.Name = "btn_addTaskRepSoftware";
            this.btn_addTaskRepSoftware.Size = new System.Drawing.Size(51, 47);
            this.btn_addTaskRepSoftware.ToolTipText = "Aggiungi Task";
            this.btn_addTaskRepSoftware.Click += new System.EventHandler(this.btn_addTaskRepSoftware_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 50);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton2.Text = "Clona Record";
            this.toolStripButton2.Visible = false;
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator8.Visible = false;
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton3.Text = "Evidenzia";
            this.toolStripButton3.ToolTipText = "Evidenzia";
            this.toolStripButton3.Visible = false;
            // 
            // btn_delTaskRepSoftware
            // 
            this.btn_delTaskRepSoftware.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_delTaskRepSoftware.Image = ((System.Drawing.Image)(resources.GetObject("btn_delTaskRepSoftware.Image")));
            this.btn_delTaskRepSoftware.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_delTaskRepSoftware.Name = "btn_delTaskRepSoftware";
            this.btn_delTaskRepSoftware.Size = new System.Drawing.Size(51, 47);
            this.btn_delTaskRepSoftware.Text = "toolStripButton2";
            this.btn_delTaskRepSoftware.ToolTipText = "Rimuovi Task";
            this.btn_delTaskRepSoftware.Click += new System.EventHandler(this.btn_delTaskRepSoftware_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 50);
            // 
            // tstb_SearchTaskRepSoftware
            // 
            this.tstb_SearchTaskRepSoftware.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tstb_SearchTaskRepSoftware.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tstb_SearchTaskRepSoftware.Name = "tstb_SearchTaskRepSoftware";
            this.tstb_SearchTaskRepSoftware.Size = new System.Drawing.Size(100, 50);
            this.tstb_SearchTaskRepSoftware.Text = "Cerca...";
            this.tstb_SearchTaskRepSoftware.ToolTipText = "Cerca Task";
            this.tstb_SearchTaskRepSoftware.Leave += new System.EventHandler(this.tstb_SearchTaskRepSoftware_Leave);
            this.tstb_SearchTaskRepSoftware.Click += new System.EventHandler(this.tstb_SearchTaskRepSoftware_Click);
            this.tstb_SearchTaskRepSoftware.TextChanged += new System.EventHandler(this.tstb_SearchTaskRepSoftware_TextChanged);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_exportTaskRepSoftware
            // 
            this.btn_exportTaskRepSoftware.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_exportTaskRepSoftware.Image = ((System.Drawing.Image)(resources.GetObject("btn_exportTaskRepSoftware.Image")));
            this.btn_exportTaskRepSoftware.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_exportTaskRepSoftware.Name = "btn_exportTaskRepSoftware";
            this.btn_exportTaskRepSoftware.Size = new System.Drawing.Size(51, 47);
            this.btn_exportTaskRepSoftware.Text = "toolStripButton4";
            this.btn_exportTaskRepSoftware.ToolTipText = "Esporta Lista Task";
            this.btn_exportTaskRepSoftware.Click += new System.EventHandler(this.btn_exportTaskRepSoftware_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton6.ToolTipText = "Stampa Test";
            this.toolStripButton6.Visible = false;
            // 
            // tabQualita
            // 
            this.tabQualita.Controls.Add(this.dgv_taskRepQualita);
            this.tabQualita.Controls.Add(this.toolStrip7);
            this.tabQualita.ImageIndex = 8;
            this.tabQualita.Location = new System.Drawing.Point(4, 60);
            this.tabQualita.Name = "tabQualita";
            this.tabQualita.Size = new System.Drawing.Size(1234, 398);
            this.tabQualita.TabIndex = 5;
            this.tabQualita.Tag = "Qualita";
            this.tabQualita.ToolTipText = "Qualità";
            this.tabQualita.UseVisualStyleBackColor = true;
            // 
            // dgv_taskRepQualita
            // 
            this.dgv_taskRepQualita.AllowUserToAddRows = false;
            this.dgv_taskRepQualita.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_taskRepQualita.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_taskRepQualita.Location = new System.Drawing.Point(0, 50);
            this.dgv_taskRepQualita.Name = "dgv_taskRepQualita";
            this.dgv_taskRepQualita.ReadOnly = true;
            this.dgv_taskRepQualita.Size = new System.Drawing.Size(1234, 348);
            this.dgv_taskRepQualita.TabIndex = 9;
            this.dgv_taskRepQualita.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_taskRepQualita_CellClick);
            this.dgv_taskRepQualita.Sorted += new System.EventHandler(this.dgv_taskRepQualita_Sorted);
            // 
            // toolStrip7
            // 
            this.toolStrip7.AutoSize = false;
            this.toolStrip7.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip7.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip7.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_addTaskRepQualita,
            this.toolStripButton19,
            this.toolStripSeparator26,
            this.toolStripButton20,
            this.toolStripSeparator27,
            this.btn_delTaskRepQualita,
            this.toolStripSeparator28,
            this.tstb_SearchTaskRepQualita,
            this.toolStripSeparator29,
            this.btn_exportTaskRepQualita,
            this.toolStripButton24});
            this.toolStrip7.Location = new System.Drawing.Point(0, 0);
            this.toolStrip7.Name = "toolStrip7";
            this.toolStrip7.Size = new System.Drawing.Size(1234, 50);
            this.toolStrip7.TabIndex = 4;
            this.toolStrip7.Text = "toolStrip7";
            // 
            // btn_addTaskRepQualita
            // 
            this.btn_addTaskRepQualita.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_addTaskRepQualita.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_addTaskRepQualita.Image = ((System.Drawing.Image)(resources.GetObject("btn_addTaskRepQualita.Image")));
            this.btn_addTaskRepQualita.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_addTaskRepQualita.Name = "btn_addTaskRepQualita";
            this.btn_addTaskRepQualita.Size = new System.Drawing.Size(51, 47);
            this.btn_addTaskRepQualita.ToolTipText = "Aggiungi Task";
            this.btn_addTaskRepQualita.Click += new System.EventHandler(this.btn_addTaskRepQualita_Click);
            // 
            // toolStripButton19
            // 
            this.toolStripButton19.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton19.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton19.Image")));
            this.toolStripButton19.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton19.Name = "toolStripButton19";
            this.toolStripButton19.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton19.Text = "Clona Record";
            this.toolStripButton19.Visible = false;
            // 
            // toolStripSeparator26
            // 
            this.toolStripSeparator26.Name = "toolStripSeparator26";
            this.toolStripSeparator26.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator26.Visible = false;
            // 
            // toolStripButton20
            // 
            this.toolStripButton20.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton20.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton20.Image")));
            this.toolStripButton20.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton20.Name = "toolStripButton20";
            this.toolStripButton20.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton20.Text = "Evidenzia";
            this.toolStripButton20.ToolTipText = "Evidenzia";
            this.toolStripButton20.Visible = false;
            // 
            // toolStripSeparator27
            // 
            this.toolStripSeparator27.Name = "toolStripSeparator27";
            this.toolStripSeparator27.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_delTaskRepQualita
            // 
            this.btn_delTaskRepQualita.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_delTaskRepQualita.Image = ((System.Drawing.Image)(resources.GetObject("btn_delTaskRepQualita.Image")));
            this.btn_delTaskRepQualita.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_delTaskRepQualita.Name = "btn_delTaskRepQualita";
            this.btn_delTaskRepQualita.Size = new System.Drawing.Size(51, 47);
            this.btn_delTaskRepQualita.Text = "toolStripButton2";
            this.btn_delTaskRepQualita.ToolTipText = "Rimuovi Task";
            this.btn_delTaskRepQualita.Click += new System.EventHandler(this.btn_delTaskRepQualita_Click);
            // 
            // toolStripSeparator28
            // 
            this.toolStripSeparator28.Name = "toolStripSeparator28";
            this.toolStripSeparator28.Size = new System.Drawing.Size(6, 50);
            // 
            // tstb_SearchTaskRepQualita
            // 
            this.tstb_SearchTaskRepQualita.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tstb_SearchTaskRepQualita.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tstb_SearchTaskRepQualita.Name = "tstb_SearchTaskRepQualita";
            this.tstb_SearchTaskRepQualita.Size = new System.Drawing.Size(100, 50);
            this.tstb_SearchTaskRepQualita.Text = "Cerca...";
            this.tstb_SearchTaskRepQualita.ToolTipText = "Cerca Task";
            this.tstb_SearchTaskRepQualita.Leave += new System.EventHandler(this.tstb_SearchTaskRepQualita_Leave);
            this.tstb_SearchTaskRepQualita.Click += new System.EventHandler(this.tstb_SearchTaskRepQualita_Click);
            this.tstb_SearchTaskRepQualita.TextChanged += new System.EventHandler(this.tstb_SearchTaskRepQualita_TextChanged);
            // 
            // toolStripSeparator29
            // 
            this.toolStripSeparator29.Name = "toolStripSeparator29";
            this.toolStripSeparator29.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_exportTaskRepQualita
            // 
            this.btn_exportTaskRepQualita.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_exportTaskRepQualita.Image = ((System.Drawing.Image)(resources.GetObject("btn_exportTaskRepQualita.Image")));
            this.btn_exportTaskRepQualita.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_exportTaskRepQualita.Name = "btn_exportTaskRepQualita";
            this.btn_exportTaskRepQualita.Size = new System.Drawing.Size(51, 47);
            this.btn_exportTaskRepQualita.Text = "toolStripButton4";
            this.btn_exportTaskRepQualita.ToolTipText = "Esporta Lista Task";
            this.btn_exportTaskRepQualita.Click += new System.EventHandler(this.btn_exportTaskRepQualita_Click);
            // 
            // toolStripButton24
            // 
            this.toolStripButton24.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton24.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton24.Image")));
            this.toolStripButton24.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton24.Name = "toolStripButton24";
            this.toolStripButton24.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton24.ToolTipText = "Stampa Test";
            this.toolStripButton24.Visible = false;
            // 
            // tabCompletati
            // 
            this.tabCompletati.Controls.Add(this.dgv_taskCompleted);
            this.tabCompletati.Controls.Add(this.toolStrip6);
            this.tabCompletati.ImageIndex = 9;
            this.tabCompletati.Location = new System.Drawing.Point(4, 60);
            this.tabCompletati.Name = "tabCompletati";
            this.tabCompletati.Size = new System.Drawing.Size(1234, 398);
            this.tabCompletati.TabIndex = 6;
            this.tabCompletati.Tag = "Completati";
            this.tabCompletati.ToolTipText = "Completati";
            this.tabCompletati.UseVisualStyleBackColor = true;
            // 
            // dgv_taskCompleted
            // 
            this.dgv_taskCompleted.AllowUserToAddRows = false;
            this.dgv_taskCompleted.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_taskCompleted.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_taskCompleted.Location = new System.Drawing.Point(0, 50);
            this.dgv_taskCompleted.Name = "dgv_taskCompleted";
            this.dgv_taskCompleted.ReadOnly = true;
            this.dgv_taskCompleted.Size = new System.Drawing.Size(1234, 348);
            this.dgv_taskCompleted.TabIndex = 11;
            this.dgv_taskCompleted.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_taskCompleted_CellClick);
            this.dgv_taskCompleted.Sorted += new System.EventHandler(this.dgv_taskCompleted_Sorted);
            // 
            // toolStrip6
            // 
            this.toolStrip6.AutoSize = false;
            this.toolStrip6.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip6.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip6.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton7,
            this.toolStripSeparator22,
            this.toolStripButton10,
            this.toolStripSeparator23,
            this.toolStripButton11,
            this.toolStripButton13,
            this.toolStripSeparator24,
            this.tstb_SearchTaskCompleted,
            this.toolStripSeparator25,
            this.btn_exportTaskCompleted,
            this.toolStripButton17});
            this.toolStrip6.Location = new System.Drawing.Point(0, 0);
            this.toolStrip6.Name = "toolStrip6";
            this.toolStrip6.Size = new System.Drawing.Size(1234, 50);
            this.toolStrip6.TabIndex = 10;
            this.toolStrip6.Text = "toolStrip6";
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton7.ToolTipText = "Aggiungi Task";
            this.toolStripButton7.Visible = false;
            // 
            // toolStripSeparator22
            // 
            this.toolStripSeparator22.Name = "toolStripSeparator22";
            this.toolStripSeparator22.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator22.Visible = false;
            // 
            // toolStripButton10
            // 
            this.toolStripButton10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton10.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton10.Image")));
            this.toolStripButton10.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton10.Name = "toolStripButton10";
            this.toolStripButton10.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton10.Text = "Clona Record";
            this.toolStripButton10.Visible = false;
            // 
            // toolStripSeparator23
            // 
            this.toolStripSeparator23.Name = "toolStripSeparator23";
            this.toolStripSeparator23.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator23.Visible = false;
            // 
            // toolStripButton11
            // 
            this.toolStripButton11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton11.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton11.Image")));
            this.toolStripButton11.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton11.Name = "toolStripButton11";
            this.toolStripButton11.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton11.Text = "Evidenzia";
            this.toolStripButton11.ToolTipText = "Evidenzia";
            this.toolStripButton11.Visible = false;
            // 
            // toolStripButton13
            // 
            this.toolStripButton13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton13.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton13.Image")));
            this.toolStripButton13.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton13.Name = "toolStripButton13";
            this.toolStripButton13.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton13.Text = "toolStripButton2";
            this.toolStripButton13.ToolTipText = "Rimuovi Task";
            this.toolStripButton13.Visible = false;
            // 
            // toolStripSeparator24
            // 
            this.toolStripSeparator24.Name = "toolStripSeparator24";
            this.toolStripSeparator24.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator24.Visible = false;
            // 
            // tstb_SearchTaskCompleted
            // 
            this.tstb_SearchTaskCompleted.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tstb_SearchTaskCompleted.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tstb_SearchTaskCompleted.Name = "tstb_SearchTaskCompleted";
            this.tstb_SearchTaskCompleted.Size = new System.Drawing.Size(100, 50);
            this.tstb_SearchTaskCompleted.Text = "Cerca...";
            this.tstb_SearchTaskCompleted.ToolTipText = "Cerca Task";
            this.tstb_SearchTaskCompleted.Leave += new System.EventHandler(this.tstb_SearchTaskCompleted_Leave);
            this.tstb_SearchTaskCompleted.Click += new System.EventHandler(this.tstb_SearchTaskCompleted_Click);
            this.tstb_SearchTaskCompleted.TextChanged += new System.EventHandler(this.tstb_SearchTaskCompleted_TextChanged);
            // 
            // toolStripSeparator25
            // 
            this.toolStripSeparator25.Name = "toolStripSeparator25";
            this.toolStripSeparator25.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_exportTaskCompleted
            // 
            this.btn_exportTaskCompleted.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_exportTaskCompleted.Image = ((System.Drawing.Image)(resources.GetObject("btn_exportTaskCompleted.Image")));
            this.btn_exportTaskCompleted.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_exportTaskCompleted.Name = "btn_exportTaskCompleted";
            this.btn_exportTaskCompleted.Size = new System.Drawing.Size(51, 47);
            this.btn_exportTaskCompleted.Text = "toolStripButton4";
            this.btn_exportTaskCompleted.ToolTipText = "Esporta Lista Task";
            this.btn_exportTaskCompleted.Click += new System.EventHandler(this.btn_exportTaskCompleted_Click);
            // 
            // toolStripButton17
            // 
            this.toolStripButton17.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton17.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton17.Image")));
            this.toolStripButton17.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton17.Name = "toolStripButton17";
            this.toolStripButton17.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton17.ToolTipText = "Stampa Test";
            this.toolStripButton17.Visible = false;
            // 
            // tabOverview
            // 
            this.tabOverview.Controls.Add(this.dgv_taskAll);
            this.tabOverview.Controls.Add(this.toolStrip5);
            this.tabOverview.ImageIndex = 7;
            this.tabOverview.Location = new System.Drawing.Point(4, 60);
            this.tabOverview.Name = "tabOverview";
            this.tabOverview.Size = new System.Drawing.Size(1234, 398);
            this.tabOverview.TabIndex = 4;
            this.tabOverview.Tag = "Overview";
            this.tabOverview.ToolTipText = "Overview";
            this.tabOverview.UseVisualStyleBackColor = true;
            // 
            // dgv_taskAll
            // 
            this.dgv_taskAll.AllowUserToAddRows = false;
            this.dgv_taskAll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_taskAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_taskAll.Location = new System.Drawing.Point(0, 50);
            this.dgv_taskAll.Name = "dgv_taskAll";
            this.dgv_taskAll.ReadOnly = true;
            this.dgv_taskAll.Size = new System.Drawing.Size(1234, 348);
            this.dgv_taskAll.TabIndex = 9;
            this.dgv_taskAll.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_taskAll_CellClick);
            this.dgv_taskAll.Sorted += new System.EventHandler(this.dgv_taskAll_Sorted);
            // 
            // toolStrip5
            // 
            this.toolStrip5.AutoSize = false;
            this.toolStrip5.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip5.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip5.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_addTaskAll,
            this.toolStripSeparator12,
            this.toolStripButton4,
            this.toolStripSeparator19,
            this.toolStripButton5,
            this.btn_delTaskAll,
            this.toolStripSeparator20,
            this.tstb_SearchTaskAll,
            this.toolStripSeparator21,
            this.btn_exportTaskAll,
            this.toolStripButton21});
            this.toolStrip5.Location = new System.Drawing.Point(0, 0);
            this.toolStrip5.Name = "toolStrip5";
            this.toolStrip5.Size = new System.Drawing.Size(1234, 50);
            this.toolStrip5.TabIndex = 5;
            this.toolStrip5.Text = "toolStrip5";
            // 
            // btn_addTaskAll
            // 
            this.btn_addTaskAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_addTaskAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_addTaskAll.Image = ((System.Drawing.Image)(resources.GetObject("btn_addTaskAll.Image")));
            this.btn_addTaskAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_addTaskAll.Name = "btn_addTaskAll";
            this.btn_addTaskAll.Size = new System.Drawing.Size(51, 47);
            this.btn_addTaskAll.ToolTipText = "Aggiungi Task";
            this.btn_addTaskAll.Visible = false;
            this.btn_addTaskAll.Click += new System.EventHandler(this.btn_addTaskAll_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator12.Visible = false;
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton4.Text = "Clona Record";
            this.toolStripButton4.Visible = false;
            // 
            // toolStripSeparator19
            // 
            this.toolStripSeparator19.Name = "toolStripSeparator19";
            this.toolStripSeparator19.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator19.Visible = false;
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton5.Text = "Evidenzia";
            this.toolStripButton5.ToolTipText = "Evidenzia";
            this.toolStripButton5.Visible = false;
            // 
            // btn_delTaskAll
            // 
            this.btn_delTaskAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_delTaskAll.Image = ((System.Drawing.Image)(resources.GetObject("btn_delTaskAll.Image")));
            this.btn_delTaskAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_delTaskAll.Name = "btn_delTaskAll";
            this.btn_delTaskAll.Size = new System.Drawing.Size(51, 47);
            this.btn_delTaskAll.Text = "toolStripButton2";
            this.btn_delTaskAll.ToolTipText = "Rimuovi Task";
            this.btn_delTaskAll.Visible = false;
            this.btn_delTaskAll.Click += new System.EventHandler(this.btn_delTaskAll_Click);
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator20.Visible = false;
            // 
            // tstb_SearchTaskAll
            // 
            this.tstb_SearchTaskAll.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tstb_SearchTaskAll.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tstb_SearchTaskAll.Name = "tstb_SearchTaskAll";
            this.tstb_SearchTaskAll.Size = new System.Drawing.Size(100, 50);
            this.tstb_SearchTaskAll.Text = "Cerca...";
            this.tstb_SearchTaskAll.ToolTipText = "Cerca Task";
            this.tstb_SearchTaskAll.Leave += new System.EventHandler(this.tstb_SearchTaskAll_Leave);
            this.tstb_SearchTaskAll.Click += new System.EventHandler(this.tstb_SearchTaskAll_Click);
            this.tstb_SearchTaskAll.TextChanged += new System.EventHandler(this.tstb_SearchTaskAll_TextChanged);
            // 
            // toolStripSeparator21
            // 
            this.toolStripSeparator21.Name = "toolStripSeparator21";
            this.toolStripSeparator21.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_exportTaskAll
            // 
            this.btn_exportTaskAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_exportTaskAll.Image = ((System.Drawing.Image)(resources.GetObject("btn_exportTaskAll.Image")));
            this.btn_exportTaskAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_exportTaskAll.Name = "btn_exportTaskAll";
            this.btn_exportTaskAll.Size = new System.Drawing.Size(51, 47);
            this.btn_exportTaskAll.Text = "toolStripButton4";
            this.btn_exportTaskAll.ToolTipText = "Esporta Lista Task";
            this.btn_exportTaskAll.Click += new System.EventHandler(this.btn_exportTaskAll_Click);
            // 
            // toolStripButton21
            // 
            this.toolStripButton21.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton21.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton21.Image")));
            this.toolStripButton21.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton21.Name = "toolStripButton21";
            this.toolStripButton21.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton21.ToolTipText = "Stampa Test";
            this.toolStripButton21.Visible = false;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "boards.png");
            this.imageList.Images.SetKeyName(1, "clients.png");
            this.imageList.Images.SetKeyName(2, "repairs.png");
            this.imageList.Images.SetKeyName(3, "settings.png");
            this.imageList.Images.SetKeyName(4, "information.png");
            this.imageList.Images.SetKeyName(5, "technicalDrawing.png");
            this.imageList.Images.SetKeyName(6, "software.png");
            this.imageList.Images.SetKeyName(7, "ToDoList.png");
            this.imageList.Images.SetKeyName(8, "qualityAssurance.png");
            this.imageList.Images.SetKeyName(9, "taskCompleted.png");
            this.imageList.Images.SetKeyName(10, "bulb-icon.png");
            this.imageList.Images.SetKeyName(11, "task-notes-icon.png");
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 50);
            // 
            // lbl_NomeUtente
            // 
            this.lbl_NomeUtente.Name = "lbl_NomeUtente";
            this.lbl_NomeUtente.Size = new System.Drawing.Size(78, 47);
            this.lbl_NomeUtente.Text = "Nome Utente";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_AddMagazzino
            // 
            this.btn_AddMagazzino.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_AddMagazzino.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_AddMagazzino.Image = ((System.Drawing.Image)(resources.GetObject("btn_AddMagazzino.Image")));
            this.btn_AddMagazzino.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_AddMagazzino.Name = "btn_AddMagazzino";
            this.btn_AddMagazzino.Size = new System.Drawing.Size(51, 47);
            this.btn_AddMagazzino.ToolTipText = "Aggiungi A Magazzino";
            this.btn_AddMagazzino.Visible = false;
            // 
            // btn_CloneMagazzino
            // 
            this.btn_CloneMagazzino.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_CloneMagazzino.Image = ((System.Drawing.Image)(resources.GetObject("btn_CloneMagazzino.Image")));
            this.btn_CloneMagazzino.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_CloneMagazzino.Name = "btn_CloneMagazzino";
            this.btn_CloneMagazzino.Size = new System.Drawing.Size(51, 47);
            this.btn_CloneMagazzino.Text = "Clona Record";
            this.btn_CloneMagazzino.Visible = false;
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator10.Visible = false;
            // 
            // btn_ColorLines
            // 
            this.btn_ColorLines.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_ColorLines.Image = ((System.Drawing.Image)(resources.GetObject("btn_ColorLines.Image")));
            this.btn_ColorLines.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_ColorLines.Name = "btn_ColorLines";
            this.btn_ColorLines.Size = new System.Drawing.Size(51, 47);
            this.btn_ColorLines.Text = "Evidenzia";
            this.btn_ColorLines.ToolTipText = "Evidenzia";
            this.btn_ColorLines.Visible = false;
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator3.Visible = false;
            // 
            // btn_RemoveMagazzino
            // 
            this.btn_RemoveMagazzino.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_RemoveMagazzino.Image = ((System.Drawing.Image)(resources.GetObject("btn_RemoveMagazzino.Image")));
            this.btn_RemoveMagazzino.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_RemoveMagazzino.Name = "btn_RemoveMagazzino";
            this.btn_RemoveMagazzino.Size = new System.Drawing.Size(51, 47);
            this.btn_RemoveMagazzino.Text = "toolStripButton2";
            this.btn_RemoveMagazzino.ToolTipText = "Rimuovi Da Magazzino";
            this.btn_RemoveMagazzino.Visible = false;
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator11.Visible = false;
            // 
            // btn_ExportMagazzino
            // 
            this.btn_ExportMagazzino.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_ExportMagazzino.Image = ((System.Drawing.Image)(resources.GetObject("btn_ExportMagazzino.Image")));
            this.btn_ExportMagazzino.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_ExportMagazzino.Name = "btn_ExportMagazzino";
            this.btn_ExportMagazzino.Size = new System.Drawing.Size(51, 47);
            this.btn_ExportMagazzino.Text = "toolStripButton4";
            this.btn_ExportMagazzino.ToolTipText = "Esporta Magazzino";
            this.btn_ExportMagazzino.Visible = false;
            // 
            // btn_PrintTestFiltro
            // 
            this.btn_PrintTestFiltro.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_PrintTestFiltro.Image = ((System.Drawing.Image)(resources.GetObject("btn_PrintTestFiltro.Image")));
            this.btn_PrintTestFiltro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_PrintTestFiltro.Name = "btn_PrintTestFiltro";
            this.btn_PrintTestFiltro.Size = new System.Drawing.Size(51, 47);
            this.btn_PrintTestFiltro.ToolTipText = "Stampa Test";
            this.btn_PrintTestFiltro.Visible = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator2,
            this.lbl_NomeUtente,
            this.toolStripSeparator1,
            this.btn_Updates,
            this.btn_AddMagazzino,
            this.btn_CloneMagazzino,
            this.toolStripSeparator10,
            this.btn_ColorLines,
            this.toolStripSeparator3,
            this.btn_RemoveMagazzino,
            this.toolStripSeparator11,
            this.btn_ExportMagazzino,
            this.btn_PrintTestFiltro});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1242, 50);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btn_Updates
            // 
            this.btn_Updates.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_Updates.Image = ((System.Drawing.Image)(resources.GetObject("btn_Updates.Image")));
            this.btn_Updates.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Updates.Name = "btn_Updates";
            this.btn_Updates.Size = new System.Drawing.Size(51, 47);
            this.btn_Updates.Text = "toolStripButton7";
            this.btn_Updates.ToolTipText = "Aggiorna Software";
            this.btn_Updates.Visible = false;
            this.btn_Updates.Click += new System.EventHandler(this.btn_Updates_Click);
            // 
            // tabNotePersonali
            // 
            this.tabNotePersonali.Controls.Add(this.dgv_NotePersonali);
            this.tabNotePersonali.Controls.Add(this.toolStrip9);
            this.tabNotePersonali.ImageIndex = 11;
            this.tabNotePersonali.Location = new System.Drawing.Point(4, 60);
            this.tabNotePersonali.Name = "tabNotePersonali";
            this.tabNotePersonali.Size = new System.Drawing.Size(1234, 398);
            this.tabNotePersonali.TabIndex = 8;
            this.tabNotePersonali.Tag = "Note Personali";
            this.tabNotePersonali.ToolTipText = "Note Personali";
            this.tabNotePersonali.UseVisualStyleBackColor = true;
            // 
            // toolStrip9
            // 
            this.toolStrip9.AutoSize = false;
            this.toolStrip9.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip9.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip9.ImageScalingSize = new System.Drawing.Size(47, 47);
            this.toolStrip9.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_addNotePersonali,
            this.toolStripButton25,
            this.toolStripSeparator34,
            this.toolStripButton26,
            this.toolStripSeparator35,
            this.btn_delNotePersonali,
            this.toolStripSeparator36,
            this.tstb_SearchNotePersonali,
            this.toolStripSeparator37,
            this.btn_exportNotePersonali,
            this.toolStripButton30});
            this.toolStrip9.Location = new System.Drawing.Point(0, 0);
            this.toolStrip9.Name = "toolStrip9";
            this.toolStrip9.Size = new System.Drawing.Size(1234, 50);
            this.toolStrip9.TabIndex = 5;
            this.toolStrip9.Text = "toolStrip9";
            // 
            // btn_addNotePersonali
            // 
            this.btn_addNotePersonali.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_addNotePersonali.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_addNotePersonali.Image = ((System.Drawing.Image)(resources.GetObject("btn_addNotePersonali.Image")));
            this.btn_addNotePersonali.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_addNotePersonali.Name = "btn_addNotePersonali";
            this.btn_addNotePersonali.Size = new System.Drawing.Size(51, 47);
            this.btn_addNotePersonali.ToolTipText = "Aggiungi Task";
            this.btn_addNotePersonali.Click += new System.EventHandler(this.btn_addNotePersonali_Click);
            // 
            // toolStripButton25
            // 
            this.toolStripButton25.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton25.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton25.Image")));
            this.toolStripButton25.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton25.Name = "toolStripButton25";
            this.toolStripButton25.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton25.Text = "Clona Record";
            this.toolStripButton25.Visible = false;
            // 
            // toolStripSeparator34
            // 
            this.toolStripSeparator34.Name = "toolStripSeparator34";
            this.toolStripSeparator34.Size = new System.Drawing.Size(6, 50);
            this.toolStripSeparator34.Visible = false;
            // 
            // toolStripButton26
            // 
            this.toolStripButton26.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton26.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton26.Image")));
            this.toolStripButton26.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton26.Name = "toolStripButton26";
            this.toolStripButton26.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton26.Text = "Evidenzia";
            this.toolStripButton26.ToolTipText = "Evidenzia";
            this.toolStripButton26.Visible = false;
            // 
            // toolStripSeparator35
            // 
            this.toolStripSeparator35.Name = "toolStripSeparator35";
            this.toolStripSeparator35.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_delNotePersonali
            // 
            this.btn_delNotePersonali.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_delNotePersonali.Image = ((System.Drawing.Image)(resources.GetObject("btn_delNotePersonali.Image")));
            this.btn_delNotePersonali.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_delNotePersonali.Name = "btn_delNotePersonali";
            this.btn_delNotePersonali.Size = new System.Drawing.Size(51, 47);
            this.btn_delNotePersonali.Text = "toolStripButton2";
            this.btn_delNotePersonali.ToolTipText = "Rimuovi Task";
            this.btn_delNotePersonali.Click += new System.EventHandler(this.btn_delNotePersonali_Click);
            // 
            // toolStripSeparator36
            // 
            this.toolStripSeparator36.Name = "toolStripSeparator36";
            this.toolStripSeparator36.Size = new System.Drawing.Size(6, 50);
            // 
            // tstb_SearchNotePersonali
            // 
            this.tstb_SearchNotePersonali.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.tstb_SearchNotePersonali.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.tstb_SearchNotePersonali.Name = "tstb_SearchNotePersonali";
            this.tstb_SearchNotePersonali.Size = new System.Drawing.Size(100, 50);
            this.tstb_SearchNotePersonali.Text = "Cerca...";
            this.tstb_SearchNotePersonali.ToolTipText = "Cerca Task";
            this.tstb_SearchNotePersonali.Leave += new System.EventHandler(this.tstb_SearchNotePersonali_Leave);
            this.tstb_SearchNotePersonali.Click += new System.EventHandler(this.tstb_SearchNotePersonali_Click);
            this.tstb_SearchNotePersonali.TextChanged += new System.EventHandler(this.tstb_SearchNotePersonali_TextChanged);
            // 
            // toolStripSeparator37
            // 
            this.toolStripSeparator37.Name = "toolStripSeparator37";
            this.toolStripSeparator37.Size = new System.Drawing.Size(6, 50);
            // 
            // btn_exportNotePersonali
            // 
            this.btn_exportNotePersonali.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btn_exportNotePersonali.Image = ((System.Drawing.Image)(resources.GetObject("btn_exportNotePersonali.Image")));
            this.btn_exportNotePersonali.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_exportNotePersonali.Name = "btn_exportNotePersonali";
            this.btn_exportNotePersonali.Size = new System.Drawing.Size(51, 47);
            this.btn_exportNotePersonali.Text = "toolStripButton4";
            this.btn_exportNotePersonali.ToolTipText = "Esporta Lista Task";
            this.btn_exportNotePersonali.Click += new System.EventHandler(this.btn_exportNotePersonali_Click);
            // 
            // toolStripButton30
            // 
            this.toolStripButton30.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton30.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton30.Image")));
            this.toolStripButton30.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton30.Name = "toolStripButton30";
            this.toolStripButton30.Size = new System.Drawing.Size(51, 47);
            this.toolStripButton30.ToolTipText = "Stampa Test";
            this.toolStripButton30.Visible = false;
            // 
            // dgv_NotePersonali
            // 
            this.dgv_NotePersonali.AllowUserToAddRows = false;
            this.dgv_NotePersonali.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_NotePersonali.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_NotePersonali.Location = new System.Drawing.Point(0, 50);
            this.dgv_NotePersonali.Name = "dgv_NotePersonali";
            this.dgv_NotePersonali.ReadOnly = true;
            this.dgv_NotePersonali.Size = new System.Drawing.Size(1234, 348);
            this.dgv_NotePersonali.TabIndex = 10;
            this.dgv_NotePersonali.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_NotePersonali_CellClick);
            this.dgv_NotePersonali.Sorted += new System.EventHandler(this.dgv_NotePersonali_Sorted);
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 512);
            this.Controls.Add(this.tabControlList);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Main";
            this.Text = "ToDoList";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Main_FormClosing);
            this.tabControlList.ResumeLayout(false);
            this.tabNuoviProgetti.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_taskNewProject)).EndInit();
            this.toolStrip8.ResumeLayout(false);
            this.toolStrip8.PerformLayout();
            this.tabMeccanica.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_taskRepMeccanico)).EndInit();
            this.toolStrip4.ResumeLayout(false);
            this.toolStrip4.PerformLayout();
            this.tabElettrica.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_taskRepElettrico)).EndInit();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.tabSoftware.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_taskRepSoftware)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.tabQualita.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_taskRepQualita)).EndInit();
            this.toolStrip7.ResumeLayout(false);
            this.toolStrip7.PerformLayout();
            this.tabCompletati.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_taskCompleted)).EndInit();
            this.toolStrip6.ResumeLayout(false);
            this.toolStrip6.PerformLayout();
            this.tabOverview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_taskAll)).EndInit();
            this.toolStrip5.ResumeLayout(false);
            this.toolStrip5.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabNotePersonali.ResumeLayout(false);
            this.toolStrip9.ResumeLayout(false);
            this.toolStrip9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_NotePersonali)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlList;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel lbl_NomeUtente;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btn_AddMagazzino;
        private System.Windows.Forms.ToolStripButton btn_CloneMagazzino;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton btn_ColorLines;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btn_RemoveMagazzino;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripButton btn_ExportMagazzino;
        private System.Windows.Forms.ToolStripButton btn_PrintTestFiltro;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.TabPage tabElettrica;
        private System.Windows.Forms.DataGridView dgv_taskRepElettrico;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton btn_addTaskRepElettrico;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton btn_delTaskRepElettrico;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripTextBox tstb_SearchTaskRepElettrico;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripButton btn_exportTaskRepElettrico;
        private System.Windows.Forms.ToolStripButton toolStripButton12;
        private System.Windows.Forms.TabPage tabMeccanica;
        private System.Windows.Forms.DataGridView dgv_taskRepMeccanico;
        private System.Windows.Forms.ToolStrip toolStrip4;
        private System.Windows.Forms.ToolStripButton btn_addTaskRepMeccanico;
        private System.Windows.Forms.ToolStripButton toolStripButton14;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripButton toolStripButton15;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripButton btn_delTaskRepMeccanico;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripTextBox tstb_SearchTaskRepMeccanico;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripButton btn_exportTaskRepMeccanico;
        private System.Windows.Forms.ToolStripButton toolStripButton18;
        private System.Windows.Forms.TabPage tabSoftware;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btn_addTaskRepSoftware;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton btn_delTaskRepSoftware;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripTextBox tstb_SearchTaskRepSoftware;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripButton btn_exportTaskRepSoftware;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.DataGridView dgv_taskRepSoftware;
        private System.Windows.Forms.TabPage tabOverview;
        private System.Windows.Forms.DataGridView dgv_taskAll;
        private System.Windows.Forms.ToolStrip toolStrip5;
        private System.Windows.Forms.ToolStripButton btn_addTaskAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator19;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton btn_delTaskAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripTextBox tstb_SearchTaskAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator21;
        private System.Windows.Forms.ToolStripButton btn_exportTaskAll;
        private System.Windows.Forms.ToolStripButton toolStripButton21;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton btn_Updates;
        private System.Windows.Forms.TabPage tabQualita;
        private System.Windows.Forms.TabPage tabCompletati;
        private System.Windows.Forms.DataGridView dgv_taskCompleted;
        private System.Windows.Forms.ToolStrip toolStrip6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator22;
        private System.Windows.Forms.ToolStripButton toolStripButton10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator23;
        private System.Windows.Forms.ToolStripButton toolStripButton11;
        private System.Windows.Forms.ToolStripButton toolStripButton13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator24;
        private System.Windows.Forms.ToolStripTextBox tstb_SearchTaskCompleted;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator25;
        private System.Windows.Forms.ToolStripButton btn_exportTaskCompleted;
        private System.Windows.Forms.ToolStripButton toolStripButton17;
        private System.Windows.Forms.DataGridView dgv_taskRepQualita;
        private System.Windows.Forms.ToolStrip toolStrip7;
        private System.Windows.Forms.ToolStripButton btn_addTaskRepQualita;
        private System.Windows.Forms.ToolStripButton toolStripButton19;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator26;
        private System.Windows.Forms.ToolStripButton toolStripButton20;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator27;
        private System.Windows.Forms.ToolStripButton btn_delTaskRepQualita;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator28;
        private System.Windows.Forms.ToolStripTextBox tstb_SearchTaskRepQualita;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator29;
        private System.Windows.Forms.ToolStripButton btn_exportTaskRepQualita;
        private System.Windows.Forms.ToolStripButton toolStripButton24;
        private System.Windows.Forms.TabPage tabNuoviProgetti;
        private System.Windows.Forms.DataGridView dgv_taskNewProject;
        private System.Windows.Forms.ToolStrip toolStrip8;
        private System.Windows.Forms.ToolStripButton btn_addTaskNewProject;
        private System.Windows.Forms.ToolStripButton toolStripButton22;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator30;
        private System.Windows.Forms.ToolStripButton toolStripButton23;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator31;
        private System.Windows.Forms.ToolStripButton btn_delTaskNewProject;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator32;
        private System.Windows.Forms.ToolStripTextBox tstb_SearchTaskNewProject;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator33;
        private System.Windows.Forms.ToolStripButton btn_exportTaskNewProject;
        private System.Windows.Forms.ToolStripButton toolStripButton27;
        private System.Windows.Forms.Timer timerTempoLavorato;
        private System.Windows.Forms.TabPage tabNotePersonali;
        private System.Windows.Forms.DataGridView dgv_NotePersonali;
        private System.Windows.Forms.ToolStrip toolStrip9;
        private System.Windows.Forms.ToolStripButton btn_addNotePersonali;
        private System.Windows.Forms.ToolStripButton toolStripButton25;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator34;
        private System.Windows.Forms.ToolStripButton toolStripButton26;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator35;
        private System.Windows.Forms.ToolStripButton btn_delNotePersonali;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator36;
        private System.Windows.Forms.ToolStripTextBox tstb_SearchNotePersonali;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator37;
        private System.Windows.Forms.ToolStripButton btn_exportNotePersonali;
        private System.Windows.Forms.ToolStripButton toolStripButton30;
    }
}

